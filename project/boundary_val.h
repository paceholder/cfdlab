#ifndef __RANDWERTE_H__
#define __RANDWERTE_H__

#include "parameters.h"


/**
 * The boundary values of the problem are set.
 */
void boundaryvalues(
	double **U,
	double **V,
	int **FlagField,
	Parameters* parameters
);

void spec_boundary_val(double **U, double **V, int **FlagField, Parameters *parameters );


#endif
