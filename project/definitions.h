#ifndef __DEFINITIONS_H__
#define __DEFINITIONS_H__

/* this macro computes the y or x position of element i with grid size dx */
#define COM_LENGTH(i,dx) ( ( (i) - 0.5 ) *  (dx) )

#define FLUID 16   /* 10000 */
#define EAST   8   /* 01000 */
#define WEST   4   /* 00100 */
#define SOUTH  2   /* 00010 */
#define NORTH  1   /* 00001 */

#define NW (NORTH | WEST)
#define NE (NORTH | EAST)
#define SW (SOUTH | WEST)
#define SE (SOUTH | EAST)

/* pressure flags */
#define PRESSURE_LEFT (64 | OUTFLOW)
#define PRESSURE_RIGHT (32 | OUTFLOW)

#define NOSLIP   (1 << 7)  /* 00001 00 00000 */
#define FREESLIP (2 << 7)  /* 00010 00 00000 */
#define OUTFLOW  (4 << 7)  /* 00100 00 00000 */

/* inflow flag */
#define INFLOW_CONST_VELOCITY ( 8 << 7 )  /* 01000 00 00000 */
#define INFLOW_SHEAR_VELOCITY ( 16 << 7 ) /* 10000 00 00000 */

/* flages for free boundary problem */
#define EMPTY (16 << 12 )    /*10000 00000 00 00000 */
#define E_EAST  ( 8 << 12 )  /*01000 00000 00 00000 */
#define E_WEST  ( 4 << 12 )  /*00100 00000 00 00000 */
#define E_SOUTH ( 2 << 12 )  /*00010 00000 00 00000 */
#define E_NORTH ( 1 << 12 )  /*00001 00000 00 00000 */

#define E_NWES ( E_NORTH | E_WEST | E_EAST | E_SOUTH )
#define E_CNWES ( E_NORTH | E_WEST | E_EAST | E_SOUTH | EMPTY)

#define E_NW (E_NORTH | E_WEST)
#define E_NE (E_NORTH | E_EAST)
#define E_SW (E_SOUTH | E_WEST)
#define E_SE (E_SOUTH | E_EAST)
#define E_NS ( E_SOUTH | E_NORTH )
#define E_EW ( E_EAST | E_WEST )

#define E_NES ( N_NORTH | E_WEST | S_SOUTH)
#define E_ESW ( E_WEST  | S_SOUTH| W_WEST)
#define E_SWN ( S_NORTH | N_WEST | W_SOUTH)
#define E_WNE ( W_NORTH | N_WEST | E_SOUTH)

#endif
