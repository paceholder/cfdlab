#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_

typedef struct
{
	double UI;
	double VI;
	double PI;

	double GX;
	double GY;

	double t_end;
	double dt;
	double dt_value;

	double alpha;
	double omg;
	double tau;
	double eps;

	int itermax;
	double Re;
	double pressure_left;
	double pressure_right;
	double wall_speed;
	double u_inflow;
	double v_inflow;
	double xlength;
	double ylength;
	int imax;
	int jmax;
	double dx;
	double dy;
	int ppc;
} Parameters;


#endif
