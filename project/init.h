#ifndef __INIT_H_
#define __INIT_H_

#include "parameters.h"

int read_parameters(
  const char *szFileName,
  char *mapFileName,
  Parameters *params
);

/**
 * The arrays U,V and P are initialized to the constant values UI, VI and PI on
 * the whole domain.
 */
void init_uvp(
	Parameters* params,
  double **U,
  double **V,
  double **P
);

void check_flag(Parameters *params, int **Flagfield, int *status);

#endif

