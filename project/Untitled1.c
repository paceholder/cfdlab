if ((FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_SOUTH) && (FLAG[i][j] & E_EAST))
			{
			    /*Calculating velocities on the surface edges*/
                    U[i-1][j] = U[i-1][j] + params->dt*params->GX;
                    U[i][j] = U[i][j] + params->dt*params->GX;
                    V[i][j-1] = V[i][j-1] + params->dt*params->GY;
                    V[i][j] = V[i][j] + params->dt*params->GY;

			    /*Pressure is set to 0*/
			    P[i][j] = 0;

			    if((FLAG[i-1][j-1] & EMPTY)==EMPTY)
			    {
			        /*The grey bottom left cell is empty*/
			        U[i-1][j-1] = U[i-1][j];
                    V[i-1][j-1] = V[i][j-1];
			    }
			    if((FLAG[i+1][j-1] & EMPTY)==EMPTY)
			    {
			        /*The grey bottom right cell is empty*/
			        U[i][j-1] = U[i][j];
                    V[i+1][j-1] = V[i][j-1];
			    }
			    if((FLAG[i-1][j+1] & EMPTY)==EMPTY)
			    {
			        /*The grey top left cell is empty*/
			        U[i-1][j+1] = U[i-1][j];
                    V[i-1][j] = V[i][j];
			    }
			    if((FLAG[i+1][j+1] & EMPTY)==EMPTY)
			    {
			        /*The grey top right cell is empty*/
			        U[i][j+1] = U[i][j];
                    V[i+1][j] = V[i][j];
			    }


			}
