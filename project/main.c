#include <stdio.h>
#include <assert.h>

#include "visual.h"
#include "helper.h"
#include "init.h"
#include "uvp.h"
#include "sor.h"
#include "boundary_val.h"
#include "helper.h"
#include "definitions.h"
#include "parameters.h"
#include "surface.h"

#define NUM_PARTICLE_LINES 1

int main(int argn, char** args)
{
    const char* outputFileName = "out.vtk";
    int status;
    char mapFileName[500];
	
	/*Particle *p;
	int i;*/

    Parameters params;
    ParticleLine particleLines[NUM_PARTICLE_LINES];
    int N; 			/* number of particle lines created in init_particle */
    int** FlagField;
    double** U;
    double** V;
    double** F;
    double** G;
    double** RS;
    double** P;

    double t = 0.0;
    int n = 0;

    particleLines[0].length = 0;

    if (argn < 2)
    {
        printf("Please pass parameters file as an argument");
        return 0;
    }


    /* first read parameters ... */
    read_parameters(args[1], mapFileName, &params);

    /* read our map which sets geometry and obstacles */
    FlagField = read_ppm(mapFileName, &params);

	/* set different combinations of flags */
    setup_flagfield(&params, FlagField);

	/* checks correctness of flagfield */
    check_flag(&params, FlagField, &status);

	if ( status == -1 )
	{
		printf("Error in Your scenario file; please check geometry again");
		return status;
	}

	/* allocation of the space  */
	U  = matrix(0, params.imax + 1, 0, params.jmax + 1);
	V  = matrix(0, params.imax + 1, 0, params.jmax + 1);
	F  = matrix(0, params.imax + 1, 0, params.jmax + 1);
	G  = matrix(0, params.imax + 1, 0, params.jmax + 1);
	RS = matrix(0, params.imax + 1, 0, params.jmax + 1);
	P  = matrix(0, params.imax + 1, 0, params.jmax + 1);

	/* assign initial values */
	init_uvp(&params, U, V, P);

	init_particles(&N, &params, FlagField, particleLines);

	while (t < params.t_end)
	{
		int it = 0;
		double res = 1.0;

        if (n % 1 == 0)
		  write_vtkFile(outputFileName, n, &params, U, V, P,particleLines,N);


		/* new value of dt */
		calculate_dt(FlagField, &params, U, V);

		/* we set just EMTPY but we have to set FLUID as well */
		mark_cells(FlagField, params.imax, params.jmax, params.dx, params.dy, N, particleLines,U,V);

		set_uvp_surface( U, V, P, FlagField, &params);

		calculate_fg(&params, U, V, F, G, FlagField);

		calculate_rs(&params, F, G, RS, FlagField);

		/* sor iteration */
		while (it < params.itermax && res > params.eps)
		{
			sor(&params, P, RS, &res, FlagField);
			++it;
		}
		
		/*
		i = 0;
		p = particleLines[0].Particles;
		while (p != NULL)
		{
			p = p->next;
			++i;
		}		
		assert(particleLines[0].length == i);
		*/

		calculate_uv(&params, FlagField, U, V, F, G, P);
		
		boundaryvalues(U, V, FlagField, &params);

		advance_particles(params.imax, params.jmax, params.dx , params.dy, params.dt, U,V,N, particleLines ,FlagField);
		
		
		/* after advancing particles we should check inflow cells and
		 * add new cells
		 */
		
		/* add_particles_inflow(particleLines, &params, FlagField); */
		

		t += params.dt;
		++n;
	}


    /* clean up memory */

    free_matrix(P, 0, params.imax+1, 0, params.jmax+1);
    free_matrix(RS, 0, params.imax+1, 0, params.jmax+1);
    free_matrix(G, 0, params.imax+1, 0, params.jmax+1);
    free_matrix(F, 0, params.imax+1, 0, params.jmax+1);
    free_matrix(V, 0, params.imax+1, 0, params.jmax+1);
    free_matrix(U, 0, params.imax+1, 0, params.jmax+1);


    return 0;
}
