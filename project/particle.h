#ifndef __PARTICLE_H__
#define __PARTICLE_H__

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "particlestruct.h"
#include "parameters.h"

void fill_cell_with_particles(int i, int j, Parameters* params, ParticleLine* pline, int** Flags);


/* The particles are dis- tributed uniformly in the problem-dependent fluid domain QQ as de- scribed at the beginning of Section 6.1. These can be held in a data structure such as particleline introduced in Section 4.2.2. Depending on the given problem, different areas of the initial configuration may be distinguished by inserting the particles into separate lists and laterdis- playing the particles in each list with a different color or symbol during visualization.
A pointer to the first particle list is returned.
The parameter ppc (particles per cell) passes the number of potential initial particle positions per cell. For square cells (6x = dy), ppc should be the square of an integer. This function is called from the initialization section ofmain. */
void init_particles(int *N, Parameters* params, int** Flags, ParticleLine* pline);

/* add a particle at the beginning of a particle line */
void add_particle( ParticleLine* pline, Particle* particle );

void add_particles_inflow(ParticleLine* pline, Parameters* params, int** FlagField);

/* remove a particle from a ParticleLine */
Particle* remove_particle( ParticleLine* pline, Particle* particle);

/* The particle positions of time tn held in Part- lines (an array of particleline) are advanced to the particle positions of time tn+i according to (4.3). */
void advance_particles(int imax,int jmax,double delx,double dely,double delt,double** U,double** V,int N,ParticleLine *Partlines,int** flags);



/* The particle positions contained in Partlines are written to the file specified in the input parameter output file. */
void write_particles(const char* outputfile,int timeStepNumber,int N, ParticleLine *Partlines);

/* Uses ADVANCE-PARTICLES to compute the current positions of the particles to be traced at each time step and overwrites their old positions in Partlines. At time intervals delt_trace, the current particle positions are appended to the file specified in the input parameter outputf ile using WRITE-PARTICLES. */
void particle_tracing(char* outputfile, double delt_trace,double t ,int imax,int jmax, int delx, int dely, int delt,double** U,double** V , int N, ParticleLine *Partlines);

#endif
