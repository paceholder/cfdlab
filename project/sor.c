#include "sor.h"
#include "definitions.h"
#include <math.h>
#include <assert.h>


void sor(
	Parameters* params,
  double **P,
  double **RS,
  double *res,
  int** flag
)
{
    int i,j;
    double rloc;
    int imax = params->imax;
    int jmax = params->jmax;
    double dx = params->dx;
    double dy = params->dy;
    double omg = params->omg;

    double coeff = omg / (2.0 * (1.0 / (dx*dx) + 1.0 / (dy*dy) ) );
    int fCellCounter = 0; /* counts the number of FLUID cells in grid domains */


    /* SOR iteration */
    for(i = 1; i <= imax; i++)
    {
        for(j = 1; j<=jmax; j++)
        {

			/* skipped the cells which are obstacles */
			if ( (flag[i][j] & FLUID) == 0 )
				continue;

			/* skipped the surface and empty cells */
			if ( (flag[i][j] & E_CNWES) != 0 )
				continue;

			P[i][j] = (1.0 - omg) * P[i][j]
					+ coeff*(( P[i+1][j]+P[i-1][j])/(dx*dx) + ( P[i][j+1]+P[i][j-1])/(dy*dy) - RS[i][j]);

		}
    }

    /* compute the residual */
    rloc = 0;
    for(i = 1; i <= imax; i++)
    {
        for(j = 1; j <= jmax; j++)
        {

          /* skipped the cells which are obstacles */
            if ( (flag[i][j]&FLUID) == 0 )
                continue;

			/* skipped the surface and empty cells */
			if ( (flag[i][j] & E_CNWES) != 0 )
				continue;

            rloc += ( (P[i+1][j] - 2.0 * P[i][j] + P[i-1][j]) / (dx*dx) + ( P[i][j+1] - 2.0 * P[i][j] + P[i][j-1]) / (dy*dy) - RS[i][j]) *
                    ( (P[i+1][j] - 2.0 * P[i][j] + P[i-1][j]) / (dx*dx) + ( P[i][j+1] - 2.0 * P[i][j] + P[i][j-1]) / (dy*dy) - RS[i][j]);

            fCellCounter++;
        }
    }
    rloc = rloc/(fCellCounter);
    rloc = sqrt(rloc);
    /* set residual */
    *res = rloc;


    /* set pressure boundary values */
    /* upper and lower walls */
    for(i = 1; i <= imax; i++)
    {
        P[i][0] = P[i][1];
        P[i][jmax+1] = P[i][jmax];
    }

    /* left and right walls */
    for(j = 1; j <= jmax; j++)
    {
        if( (flag[i][j] & PRESSURE_LEFT) > 0 )
            P[0][j] = 2 * params->pressure_left - P[1][j];
        else
            P[0][j] = P[1][j];

        if( (flag[i][j] & PRESSURE_RIGHT) > 0 )
            P[imax+1][j] = 2 * params->pressure_right - P[imax][j];
        else
            P[imax+1][j] = P[imax][j];
    }

  
    /* boundary values for in the interior boundary cells */
    for( i = 1; i <= imax ; i++ )
    {
        for( j = 1; j <= jmax ; j++ )
        {

            int oCellNCounter = 0; /* counts the number of FLUID neighbours of a obstacle */
            double pressureSum = 0.0;

            if ( (flag[i][j] & FLUID) == 0 )
            {

                /* North */
                if (flag[i][j] & NORTH )
                {
                    oCellNCounter++;
                    pressureSum += P[i][j+1];
                }

                /* South */
                if (flag[i][j] & SOUTH )
                {
                    oCellNCounter++;
                    pressureSum += P[i][j-1];
                }

                /* East */
                if( flag[i][j] & EAST )
                {
                    oCellNCounter++;
                    pressureSum += P[i+1][j];
                }

                /* West */
                if( flag[i][j] & WEST )
                {
                    oCellNCounter++;
                    pressureSum += P[i-1][j];
                }

                /* set the pressure for the obstacle cells as the average of the pressure of its surrounding FLUID cells */
                if(oCellNCounter != 0)
                    P[i][j] = pressureSum/(double)oCellNCounter;

            }
        }
    }
}

