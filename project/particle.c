#include "particle.h"
#include "definitions.h"
#include "stdio.h"
#include <math.h>
#include "helper.h"
#include "visual.h"

void fill_cell_with_particles(int i, int j, Parameters* params, ParticleLine* pline, int** Flags)
{
	int pindex;
	double xoffset;
	double yoffset;
	int n = (int)sqrt(params->ppc);	/* number of particles in each direction */
	int pi, pj;			/* x and y indices of each particle */
	double h_x = params->dx / n;
	double h_y = params->dy / n;
	
	if ( ( Flags[i][j] & EMPTY ) == 0)
	{
		for (pindex = 0; pindex < params->ppc; ++pindex)
		{
			int k = sizeof(Particle);
			Particle* particle = (Particle*) malloc(sizeof(Particle));
			k++;
			xoffset = (i-1) * params->dx;
			yoffset = (j-1) * params->dy;

			pi = pindex % n ;
			pj = pindex / n ;

			particle->x = h_x/2 + pi*h_x + xoffset;
			particle->y = h_y/2 + pj*h_y + yoffset;

			add_particle( pline , particle );
		}
	}				
}


/* The particles are dis- tributed uniformly in the problem-dependent fluid domain QQ
 * as described at the beginning of Section 6.1.
 * These can be held in a data structure such as particleline introduced in Section 4.2.2.
 * Depending on the given problem, different areas of the initial configuration may be distinguished
 * by inserting the particles into separate lists and laterdis- playing the particles
 * in each list with a different color or symbol during visualization.
 * A pointer to the first particle list is returned.
 * The parameter ppc (particles per cell) passes the number of potential initial particle positions per cell.
 * For square cells (dx = dy), ppc should be the square of an integer.
 * This function is called from the initialization section ofmain.
 */
void init_particles(int *N, Parameters *params, int** Flags, ParticleLine* pline)
{
	int i,j;
	int imax = params->imax;
	int jmax = params->jmax;	

	*N = 1;

	pline->length = 0;
	pline->Particles = NULL;

	for (i = 1; i <= imax; ++i)
	{
		for (j = 1; j <= jmax; ++j)
		{
			fill_cell_with_particles(i, j, params, pline, Flags);

		}
	}
}

/* The particle positions of time tn held in Part- lines (an array of ParticleLine) are advanced to the particle positions of time tn+i according to (4.3). */
void advance_particles(int imax,int jmax,double delx,double dely,double delt,double** U,double** V,int N, ParticleLine *partlines,int** flags)
{
	int particlelineIndex;
	int particleInex;
	Particle* particle;
	double x; 			/* x position of particle */
	double y;                     /* y position of particle */
	double pv;			/* v velocity of particle */
	double pu;			/* u velocity of particle */
	int i,j;			/* indices of the cell that contain the particle */

	/* The coordinates of the cell corners  */
	double x1,x2,y1,y2;

	/* the four staggered grid velocities */
	double u1,u2,u3,u4;
	double v1,v2,v3,v4;

	for ( particlelineIndex = 0; particlelineIndex < N; ++particlelineIndex)
	{
		particle = partlines[particlelineIndex].Particles;
		for( particleInex = 0; particleInex < partlines[particlelineIndex].length ; particleInex++)
		{
	  
			x = particle->x;
			y = particle->y;
			
			i = (int)( x/delx ) + 1;
			j = (int)((y + dely/2) / dely) + 1 ;
			
			x1 = (i-1)*delx;
			x2 = (i)*delx;
			y1 = ((j-1) - 0.5)*dely;
			y2 = (j - 0.5)*dely;
			
			u1 = U[i-1][j-1];
			u2 = U[i][j-1];
			u3 = U[i-1][j];
			u4 = U[i][j];
      
			/* computing the u velocity at postion x and y of particle */
			pu = ( (x2 - x)*(y2 - y)*u1 + (x - x1)*(y2 - y)*u2 + 
				   (x2 - x)*(y - y1)*u3 + (x - x1)*(y - y1)*u4   ) / (delx*dely);
			
			
			i = (int)( ( x + delx/2 ) / delx ) + 1;
			j = (int)(  y / dely ) + 1 ;
			
			x1 = ( (i-1) - 0.5 )*delx;
			x2 = ( i - 0.5 )*delx;
			y1 = ( j - 1 )*dely;
			y2 = j*dely;
			
			v1 = V[i-1][j-1];
			v2 = V[i][j-1];
			v3 = V[i-1][j];
			v4 = V[i][j];

			/* computing the v velocity at postion x and y of particle */
			pv = ( (x2 - x)*(y2 - y)*v1 + (x - x1)*(y2 - y)*v2 + (x2 - x)*(y - y1)*v3 + (x - x1)*(y - y1)*v4  ) / ( delx*dely );
			
			x = x + delt*pu;
			y = y + delt*pv;
			
			/* check the new position is not in obstacle or is still inside the domain */
			i = (int)(x/delx) + 1;
			j = (int)(y/dely) + 1;
			
			if ( (i < 1) || (i > imax) || (j < 1) || (j>jmax) || ((flags[i][j] & FLUID) == 0))
			{
				particle = remove_particle( (partlines+particlelineIndex) , particle );
			}
			else /* assigning the new x and y values for particle */
			{
				particle->x = x;
				particle->y = y;
				
				particle = particle->next;
			}
		}
    }
}

void add_particles_inflow(ParticleLine* pline, Parameters *params, int **FlagField)
{
	int N = 1;
	int i = 0;
	int j;	
	int particleLineIndex;
	
	int* particleCount = malloc(sizeof(int) * params->jmax);
	for(i = 0; i < params->jmax; ++i) particleCount[i] = 0;
	
	for ( particleLineIndex = 0; particleLineIndex < N; ++particleLineIndex)
	{
		Particle* particle = pline[particleLineIndex].Particles;
		while (particle != NULL)
		{
			/* computing the indices of the cell that contains the particle */
			i = (int)(particle->x/ params->dx) + 1;
			j = (int)(particle->y/ params->dy) + 1;
			
			if (i == 1 && (FlagField[0][j] & INFLOW_CONST_VELOCITY))
			{
				++particleCount[j-1];				
			}
			
			particle = particle->next;
		}
	}	
	
	for(j = 1; j <= params->jmax; ++j)
	{
		if ((FlagField[0][j] & INFLOW_CONST_VELOCITY) && (particleCount[j-1] == 0))
		{
			fill_cell_with_particles(1,j,params, pline, FlagField);
		}
	}
	
	free(particleCount);
}

void particle_tracing(char* outputfile, double delt_trace,double t ,int imax,int jmax, int delx, int dely, int delt,double** U,double** V , int N, ParticleLine *Partlines)
{
}


/* The particle positions contained in Partlines are written to the file specified in the input parameter output file. */
void write_particles(const char* outputfile,int timeStepNumber,int N, ParticleLine *partlines)
{
	char szFileName[80];
	FILE *fp = NULL;
	Particle* p = partlines->Particles;

  sprintf( szFileName, "points_%s.%i.vtk", outputfile, timeStepNumber );
	fp = fopen( szFileName, "w");
	if( fp == NULL )
	{
		char szBuff[80];
		sprintf( szBuff, "Failed to open %s", szFileName );
		ERROR( szBuff );
		return;
	}

  write_vtkHeader( fp );

  fprintf(fp,"\n");
	fprintf(fp, "DATASET UNSTRUCTURED_GRID\n");
	fprintf(fp, "POINTS %i float\n", partlines->length);


	while (p != NULL)
	{
		fprintf(fp, "%f %f %f\n", p->x, p->y, 0.0f);
		p = p->next;
	}

  if( fclose(fp) )
	{
		char szBuff[80];
		sprintf( szBuff, "Failed to close %s", szFileName );
		ERROR( szBuff );
	}

}

void add_particle( ParticleLine* pline, Particle* particle )
{
	Particle* tmpParticle = pline->Particles ;
	pline->Particles = particle;
	particle->next = tmpParticle;
	pline->length++;  
}

Particle* remove_particle(ParticleLine* pline, Particle* particle )
{
	Particle* prevP = NULL;
	Particle* curP = pline->Particles;

	while( particle != curP && curP != NULL)
	{
		prevP = curP; 
		curP = curP->next;
	}

	if ( curP == NULL )
	{
		printf( "Particle is not available in particleline ");
		return 0;
	}

	if( prevP == NULL )
	{
		pline->Particles = curP->next;
	}
	else
	{
		prevP->next = curP->next;
	}

	pline->length--;
	particle = curP->next;
  printf( "I removed one partticle at (%f,%f)\n",curP->x,curP->y);
	free(curP);

  return particle;
}
