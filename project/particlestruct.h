#ifndef __PARTICLESTRUCT_H__
#define __PARTICLESTRUCT_H__

typedef struct Particle Particle;
typedef struct ParticleLine ParticleLine;

struct Particle{
  double x,y;
  Particle* next;
};

struct ParticleLine{
  int length;
  Particle* Particles;
};


#endif 
