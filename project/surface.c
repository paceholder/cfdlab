#include "surface.h"
#include "math.h"



/* In a loop over allpar- ticles, all cells containing at least one particle are assigned the flag C_F, whereas the remaining nonobstacle cells receive the flag C_X.
This is followed by a loop over all cells which determines the surface cell types and assigns the appropriate flags (seeSection 6.1). This function is called at the beginning of the time-stepping loop in main. */
void mark_cells(int **flags,int imax,int jmax,double delx, double dely,int N, ParticleLine *partlines, double** U, double** V)
{
	int particleLineIndex;
	Particle* particle;
	int i,j;

  /* clearing the flags from last time step */
	for ( i = 1; i <= imax; ++i)
	{
		for (j = 1; j <= jmax; ++j)
		{
			flags[i][j] |= EMPTY;
			
			/* remove empty bits for east west north south */
			/* we set em later */
			flags[i][j] &= (~E_EAST);
			flags[i][j] &= (~E_WEST);
			flags[i][j] &= (~E_NORTH);
			flags[i][j] &= (~E_SOUTH);
		}
	}

	/* setting the fluid cells flags according to the particles inside the cell */
	for ( particleLineIndex = 0; particleLineIndex < N; ++particleLineIndex)
	{
		particle = partlines[particleLineIndex].Particles;
		while (particle != NULL)
		{
			/* computing the indices of the cell that contains the particle */
			i = (int)(particle->x/delx) + 1;
			j = (int)(particle->y/dely) + 1;

			flags[i][j] &= (~EMPTY) ; /* set the flag to fluid */

			particle = particle->next;
		}
	}

	/* computing the neigboring flags for each cell */
	for ( i = 1; i <= imax; ++i)
	{
		for (j = 1; j <= jmax; ++j)
		{
      int result = 0;

      /* clean the velocities if the cell is empty */
      if ( flags[i][j] & EMPTY ){
        U[i][j] = 0;
        V[i][j] = 0;
      }

			

			/* cell with fluid */
			if (flags[i-1][j] & EMPTY)
				result |= E_WEST;

			if (flags[i+1][j] & EMPTY)
				result |= E_EAST;

			if (flags[i][j-1] & EMPTY)
				result |= E_SOUTH;

			if (flags[i][j+1] & EMPTY)
				result |= E_NORTH;

			/* now check for emtpy cell */
			flags[i][j] |= result;
		}
	}
}


/* The boundary values for u, v, and p are calculated as described in Section 6.2 de- pending on the flag value of the cells. This requires first determining all velocity values after which the pressure values can be calculated. This function is called in main following MARK-CELLS. */
void set_uvp_surface(double **U,double **V,double **P,int **FLAG, Parameters* params)
{
	int i, j;
	double dx = params->dx;
	double dy = params->dy;
	/* double dt = params->dt; */
	double Re = params->Re;
	double dt = params->dt;
	double GX = params->GX;
	double GY = params->GY;


	for(i = 1; i <= params->imax; ++i)
		for(j = 1; j <= params->jmax; ++j)
		{
			if (FLAG[i][j] & EMPTY) 
				continue;
			
			/*4 empty neighbours*/
			if ((FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_SOUTH) && (FLAG[i][j] & E_EAST))
			{
			    /*Calculating velocities on the surface edges*/
				U[i-1][j] = U[i-1][j] + dt*GX;
				U[i][j] = U[i][j] + dt*GX;
				V[i][j-1] = V[i][j-1] + dt*GY;
				V[i][j] = V[i][j] + dt*GY;
				

			    if (FLAG[i-1][j-1] & EMPTY)
				{
			        /*The grey bottom left cell is empty*/
			        U[i-1][j-1] = U[i-1][j];
					V[i-1][j-1] = V[i][j-1];
				}
				if (FLAG[i+1][j-1] & EMPTY)				{
			        /*The grey bottom right cell is empty*/
			        U[i][j-1] = U[i][j];
					V[i+1][j-1] = V[i][j-1];
				}
			    if (FLAG[i-1][j+1] & EMPTY)
				{
			        /*The grey top left cell is empty*/
			        U[i-1][j+1] = U[i-1][j];
					V[i-1][j] = V[i][j];
				}
			    if (FLAG[i+1][j+1] & EMPTY)
				{
			        /*The grey top right cell is empty*/
			        U[i][j+1] = U[i][j];
					V[i+1][j] = V[i][j];
				}
			}

			/* CASE 4: THREE NEIGHBORING CELLS */
			/* NORTH WEST EAST */
			else if( (FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_EAST) )
			{

				U[i-1][j] = U[i-1][j] + dt*GX;
				U[i][j] = U[i][j] + dt*GX ;

				V[i][j] = V[i][j-1] - ( dy/dx ) * ( U[i][j] - U[i-1][j] );

				if( FLAG[i-1][j-1] & EMPTY )
				{
					V[i-1][j-1] = V[i][j-1] + ( dx/dy ) * ( U[i-1][j] - U[i-1][j-1] ); 
				} 
          
				if( FLAG[i+1][j-1] & EMPTY)
				{
					V[i+1][j-1] = V[i][j-1] - ( dx/dy ) * ( U[i][j] - U[i][j-1] );
				}

				if( FLAG[i-1][j+1] & EMPTY )
				{
					V[i-1][j] = V[i][j];
					U[i-1][j+1] = U[i-1][j];
				}

				if( FLAG[i+1][j+1] & EMPTY)
				{
					U[i][j+1] = U[i][j];
					V[i+1][j] = V[i][j];
				}
			}

			/* NORTH SOUTH WEST */
			else if( (FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_SOUTH) )
			{
				V[i][j] = V[i][j] + dt * GY;
				V[i][j-1] = V[i][j-1] + dt * GY;

				U[i-1][j] = U[i][j] + ( dx/dy ) * ( V[i][j] - V[i][j-1] ); /* should not be computed? */

				if( FLAG[i-1][j-1] & EMPTY ) 
				{
					V[i-1][j-1] = V[i][j-1];
					U[i-1][j-1] = U[i-1][j];
				}

				if( FLAG[i-1][j+1] & EMPTY )
				{
					V[i-1][j] = V[i][j];
					U[i-1][j+1] = U[i-1][j];
				}
			}

			/* NORTH SOUTH EAST */
			else if( (FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_SOUTH) && (FLAG[i][j] & E_EAST) )
			{
				
				V[i][j] = V[i][j] + dt * GY;
				V[i][j-1] = V[i][j-1] + dt * GY;
				
				U[i][j] = U[i-1][j] - ( dx/dy ) * ( V[i][j] - V[i][j-1] );
				
				if( FLAG[i-1][j-1] & EMPTY)
				{
					U[i-1][j-1] = U[i-1][j] + (dy/dx) * ( V[i][j-1] - V[i-1][j-1] );
				}
				
				if( FLAG[i-1][j+1] & EMPTY)
				{
					U[i-1][j+1] = U[i-1][j] - (dy/dx) * ( V[i][j] - V[i-1][j] );
				}
				
				if( FLAG[i+1][j-1] & EMPTY ) 
				{
					U[i][j-1] = U[i][j];
					V[i+1][j-1] = V[i][j-1];
				}
				
				if( FLAG[i+1][j+1] & EMPTY )
				{
					V[i+1][j] = V[i][j];
					U[i][j+1] = U[i][j];
				}
			}

			/* SOUTH WEST EAST */
			else if( (FLAG[i][j] & E_SOUTH) && (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_EAST) )
			{
				
				U[i-1][j] = U[i-1][j] + dt * GX;
				U[i][j] = U[i][j] + dt * GX;
				
				V[i][j-1] = V[i][j] + ( dy/dx ) * ( U[i][j] - U[i-1][j] ); /* should not be computed? */

				if( FLAG[i-1][j-1] & EMPTY ) 
				{
					U[i-1][j-1] = U[i-1][j];
					V[i-1][j-1] = V[i][j-1]; /* sould not be computed? */
				}

				if( FLAG[i+1][j-1] & EMPTY )
				{
					V[i+1][j-1] = V[i][j-1];
					U[i][j-1] = U[i][j]; /* should not be computed? */
				}
			}

		    /*Cells with two opposite empty neighbors*/
		    /*Picture 10*/
			else if (  (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_EAST) )
			{

				U[i][j] += GX * dt;
				U[i-1][j] += GX * dt;

				if (FLAG[i-1][j-1] & EMPTY)
				{
					V[i-1][j-1] = V[i][j-1] + dx/dy * (U[i-1][j] - U[i-1][j-1]);
				}

				if (FLAG[i+1][j-1] & EMPTY)
				{
					V[i+1][j-1] = V[i][j-1] - dx/dy * (U[i][j] - U[i][j-1]);
				}
			}
			/*Picture 9*/
			else if ( (FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_SOUTH) )
			{

				V[i][j] += GY * dt;
				V[i][j-1] += GY * dt;

				if ( FLAG[i-1][j+1] & EMPTY)
				{
					U[i-1][j+1] = U[i-1][j] - dy/dx * (V[i][j] - V[i-1][j]);
				}

				if ( FLAG[i-1][j-1] & EMPTY)
				{
					U[i-1][j-1] = U[i-1][j] + dy/dx * (V[i][j-1] - V[i-1][j-1]);
				}
			}
			/*Cells with two neighboring empty cells sharing a common corner*/
			/*Picture 8*/
			else if ((FLAG[i][j] & E_SOUTH) && ( FLAG[i][j] & E_WEST))
			{
				U[i-1][j] = U[i][j];
				V[i][j-1] = V[i][j];

				/*
				if (FLAG[i+1][j-1] & EMPTY)
				{
					U[i][j-1] = U[i][j] - dy/dx * (V[i+1][j-1] - V[i][j-1]);
				}*/

				/*
				if (FLAG[i-1][j+1] & EMPTY)
				{
					V[i-1][j] = V[i][j] - dx/dy * (U[i-1][j+1] - U[i-1][j]);
				}
				*/
				if (FLAG[i-1][j-1] & EMPTY)
				{
					U[i-1][j-1] = U[i-1][j];
					V[i-1][j-1] = V[i][j-1];
				}
			}
			/*Picture 6*/
			else if ((FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_WEST))
			{
				U[i-1][j] = U[i][j];
				V[i][j] = V[i][j-1];

				/* if (FLAG[i+1][j+1] & EMPTY) */
				/* { */
				/* 	U[i][j+1] = U[i][j] - params->dy/params->dx * (V[i+1][j] - V[i][j]); */
				/* } */

				if (FLAG[i-1][j-1] & EMPTY)
				{
					V[i-1][j-1] = V[i][j-1] + dx/dy * (U[i-1][j] - U[i-1][j-1]);
				}
        
				if (FLAG[i-1][j+1] & EMPTY){
					V[i-1][j] = V[i][j];
					U[i-1][j+1] = U[i-1][j];
				}

			}
			/*Picture 7*/
			else if ((FLAG[i][j] & E_SOUTH) && (FLAG[i][j] & E_EAST)) 		/* SOUTH EAST */
			{
				U[i][j] = U[i-1][j];
				V[i][j-1] = V[i][j];

				 if( FLAG[i-1][j-1] & EMPTY )
				 	{
				 		U[i-1][j-1] = U[i-1][j] + ( V[i][j-1] - V[i-1][j-1] ) * dy / dx;
				 	}

/*				if( FLAG[i+1][j+1] & EMPTY )
					{
						V[i+1][j] = V[i][j] - ( U[i][j+1] - U[i][j] ) * dx / dy;
					}*/

				 if( FLAG[i+1][j-1] & EMPTY ){
					 U[i][j-1] = U[i][j] ;
					 V[i+1][j-1] = V[i][j-1] ;
				 }
			}
			/*Picture 5*/
			else if (( FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_EAST)) /* NORTH EAST */
			{
				U[i][j] = U[i-1][j];
				V[i][j] = V[i][j-1];

				if( FLAG[i+1][j-1] & EMPTY )
				{
					V[i+1][j-1] = V[i][j-1] - ( U[i][j] - U[i][j-1] ) * dx / dy;
				}

				if( FLAG[i-1][j+1] & EMPTY )
				{
					U[i-1][j+1] = U[i-1][j] - ( V[i][j] - V[i-1][j] ) * dy / dx;
				}

				if( FLAG[i+1][j+1] & EMPTY ){
					U[i][j+1] = U[i][j];
					V[i+1][j] = V[i][j];
				}

			}
			/*Cells with one empty neighbor*/
			/*Picture 3*/
			else if (FLAG[i][j] & E_EAST) /* EAST */
			{
				U[i][j] = U[i-1][j] - dx/dy * (V[i][j] - V[i][j-1]);

				if ( FLAG[i+1][j-1] & EMPTY)
				{
					V[i+1][j-1] = V[i][j-1] - dx/dy * (U[i][j] - U[i][j-1]);
				}
			}
			/*Picture 1*/
			else if (FLAG[i][j] & E_NORTH) /* NORTH */
			{
				V[i][j] = V[i][j-1] - dy/dx * (U[i][j] - U[i-1][j]);

				if ( FLAG[i-1][j+1] & EMPTY)
				{
					U[i-1][j+1] = U[i-1][j] - dy/dx * (V[i][j] - V[i-1][j]);
				}
			}
			/*Picture 4*/
			else if (FLAG[i][j] & E_WEST) /* WEST */
			{
				U[i-1][j] = U[i][j] + dx/dy * (V[i][j] - V[i][j-1]);

				if ( FLAG[i-1][j-1] & EMPTY)
				{
					V[i-1][j-1] = V[i][j-1] + dx/dy * (U[i-1][j] - U[i-1][j-1]);
				}
			}
			/*Picture 2*/
			else if (FLAG[i][j] & E_SOUTH) /* SOUTH */
			{
				V[i][j-1] = V[i][j] - dy/dx * (U[i][j] - U[i-1][j]);
        
				if ( FLAG[i-1][j-1] & EMPTY)
				{
					U[i-1][j-1] = U[i-1][j] + dy/dx * (V[i][j-1] - V[i-1][j-1]);
				}
			}

		}


  /* COMPUTING THE PRESSURE VALUES */

	for(i = 1; i <= params->imax; ++i)
		for(j = 1; j <= params->jmax; ++j)
		{
			/*4 empty neighbours*/
			if ((FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_SOUTH) && (FLAG[i][j] & E_EAST))
			{
			    /*Pressure is set to 0*/
			    P[i][j] = 0;
			}

			/* CASE 4: THREE NEIGHBORING CELLS */
			/* NORTH WEST EAST */
			else if( (FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_EAST) )
			{
				P[i][j] = 0;
			}

			/* NORTH SOUTH WEST */
			else if( (FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_SOUTH) )
			{
				P[i][j] = 0;
			}
			
			/* NORTH SOUTH EAST */
			else if( (FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_SOUTH) && (FLAG[i][j] & E_EAST) )
			{
				P[i][j] = 0;
			}

			/* SOUTH WEST EAST */
			else if( (FLAG[i][j] & E_SOUTH) && (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_EAST) )
			{
				P[i][j] = 0;
			}

		    /*Cells with two opposite empty neighbors*/
		    /*Picture 10*/
			else if (  (FLAG[i][j] & E_WEST) && (FLAG[i][j] & E_EAST) )
			{
				P[i][j] = 0.0;

			}
			/*Picture 9*/
			else if ( (FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_SOUTH) )
			{
				P[i][j] = 0.0;
			}
			/*Cells with two neighboring empty cells sharing a common corner*/
			/*Picture 8*/
			else if ((FLAG[i][j] & E_SOUTH) && ( FLAG[i][j] & E_WEST))
			{

				P[i][j] = +( (V[i+1][j] + V[i+1][j-1] - V[i][j] - V[i][j-1])/dx +
							 (U[i][j+1] + U[i-1][j+1] - U[i][j] - U[i-1][j])/dy ) / 2 / Re;

			}
			/*Picture 6*/
			else if ((FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_WEST))
			{
				P[i][j] = -((U[i][j] + U[i-1][j] - U[i][j-1] - U[i-1][j-1])/dy +
                    ( -V[i][j] - V[i][j-1] + V[i+1][j] + V[i+1][j-1])/dx)/(2*Re);
			}
			/*Picture 7*/
			else if ((FLAG[i][j] & E_SOUTH) && (FLAG[i][j] & E_EAST)) 		/* SOUTH EAST */
			{

				P[i][j] = -( ( U[i][j+1] + U[i-1][j+1] - U[i][j] - U[i-1][j] )/dy +
										 ( V[i][j] + V[i][j-1] - V[i-1][j] - V[i-1][j-1] )/dx )/(2*Re);

			}
			/*Picture 5*/
			else if (( FLAG[i][j] & E_NORTH) && (FLAG[i][j] & E_EAST)) /* NORTH EAST */
			{
				P[i][j] = ( ( U[i][j] + U[i-1][j] - U[i][j-1] - U[i-1][j-1] )/dy +
							( V[i][j] + V[i][j-1] - V[i-1][j] - V[i-1][j-1] )/dx )/(2*Re);

			}
			/*Cells with one empty neighbor*/
			/*Picture 3*/
			else if (FLAG[i][j] & E_EAST) /* EAST */
			{
				P[i][j] = 2 / Re * (U[i][j] - U[i-1][j]) / dx;
			}
			/*Picture 1*/
			else if (FLAG[i][j] & E_NORTH) /* NORTH */
			{

				P[i][j] = 2 / Re * (V[i][j] - V[i][j-1]) / dy;
			}
			/*Picture 4*/
			else if (FLAG[i][j] & E_WEST) /* WEST */
			{

				P[i][j] = 2 / Re * (U[i][j] - U[i-1][j]) / dx;
			}
			/*Picture 2*/
			else if (FLAG[i][j] & E_SOUTH) /* SOUTH */
			{

				P[i][j] = 2 / Re * (V[i][j] - V[i][j-1]) / dy;
			}
		}
}
