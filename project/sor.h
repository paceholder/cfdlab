#ifndef __SOR_H_
#define __SOR_H_

#include "parameters.h"

/**
 * One GS iteration for the pressure Poisson equation. Besides, the routine must 
 * also set the boundary values for P according to the specification. The 
 * residual for the termination criteria has to be stored in res.
 * 
 * An \omega = 1 GS - implementation is given within sor.c.
 */
void sor(
	Parameters* params,
  double **P,
  double **RS,
  double *res,
  int** flag
);


#endif
