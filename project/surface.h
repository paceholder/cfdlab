#ifndef __SURFACE_H__
#define __SURFACE_H__

#include <stdlib.h>
#include <stddef.h>
#include "particlestruct.h"
#include "definitions.h"
#include "particle.h"
#include "parameters.h"



/* In a loop over all particles, all cells containing at least one particle are assigned the flag C_F, whereas the remaining nonobstacle cells receive the flag C_X.
This is followed by a loop over all cells which determines the surface cell types and assigns the appropriate flags (seeSection 6.1). This function is called at the beginning of the time-stepping loop in main. */
void mark_cells(int **FLAG,int imax,int jmax,double delx, double dely,int N, ParticleLine *Partlines,double** U, double** V);


/* The boun- dary values for u, v, and p are calculated as described in Section 6.2 de- pending on the flag value of the cells. This requires first determining all velocity values after which the pressure values can be calculated. This function is called in main following MARK-CELLS. */
void set_uvp_surface(double **U,double **V,double **P,int **FLAG, Parameters* params);

#endif
