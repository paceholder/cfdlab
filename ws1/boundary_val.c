#include "boundary_val.h"

/**
 * The boundary values of the problem are set.
 * @author Arash Bakhtiari
 */
void boundaryvalues(
  int imax,
  int jmax,
  double **U,
  double **V
)
{
  int j ;
  int i ;

  for (  j = 1 ; j <= jmax ; ++j)
    {
      /* Initializing the U values lying right on the left and right vertical boundaries */
      U[0][j] = 0 ;
      U[imax][j] = 0 ;

      /* Initializing the V values around the left and right vertical boundaries */
      V[0][j] = - V[1][j] ;
      V[imax+1][j] = - V[imax][j] ;
    }

  for(  i = 1; i <= imax ; i++ )
    {
      /* Initializing the V values lying right on the up and down horizontal boundaries */
      V[i][0] = 0 ;
      V[i][jmax] = 0 ;


      /* Initializing the U values around the up and down horizontal boundaries */
      U[i][0] = - U[i][1];
      /* U[i][jmax+1] = - U[i][jmax]; */

      /* here we apply new boundary conditions for moving upper lid */
      U[i][jmax+1] = 2.0 - U[i][jmax];

    } 

}

