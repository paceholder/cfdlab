#include "initLB.h"
#include "computeCellValues.h"
#include "LBDefinitions.h"

#include <string.h>

int readParameters(int *xlength, double *tau, double *velocityWall, int *timesteps, int *timestepsPerPlotting, int argc, char *argv[]){

    char* szFileName;

    /* argc 1 is a name of the program, arg 2 is information to process */
    if (argc != 2) {
        printf("Please check the argument. Program stops.");
    }

    szFileName = (argv[1]);

    READ_INT( szFileName, *xlength );

    READ_DOUBLE( szFileName, *tau    );
    READ_INT   ( szFileName, *timesteps);
    READ_INT   ( szFileName, *timestepsPerPlotting );

    read_double( szFileName, "velocityx", &velocityWall[0]);
    read_double( szFileName, "velocityy", &velocityWall[1]);
    read_double( szFileName, "velocityz", &velocityWall[2]);

    return 0;
}



void allocateMemory(double** collideField, double** streamField, int** flagField, int xlength)
{
    int size = 1;
    int i;
    const int dimensions = 3;
    for(i = 0; i < dimensions; ++i)
        size *= (xlength + 2);

    /* ------------   memory allocation -------------- */

    *collideField = (double*) malloc((size_t)( Q * size * sizeof(double)));

    *streamField = (double*) malloc((size_t)( Q * size * sizeof(double)));

    *flagField = (int *)  malloc((size_t)( size * sizeof( int )));
}



void initialiseFields(double *collideField, double *streamField, int *flagField, int xlength)
{
    int ind;
    double density = 1.0;
    double velocity[] = {0.0, 0.0, 0.0};
    double feq[Q];
    int i, j, k;

    /* ---------------- initial state for collide Field ------------------ */
    computeFeq(&density, velocity, feq);

    computeDensity(feq, &density);

    for(i = 0; i <= xlength + 1; ++i)
        for(j = 0; j <= xlength + 1; ++j)
            for(k = 0; k <= xlength + 1; ++k)
            {
                int ind = Q * INDEX(i, j, k, xlength);
                memcpy ( collideField + ind, feq, sizeof(double) * Q );

                computeDensity(collideField + ind, &density);

                density += 0.0;
            }


    /* -------------- initial state for stream Field is copied from collide field ------------ */

    memcpy(streamField, collideField, sizeof(double) * Q * (xlength + 2)* (xlength + 2) * (xlength + 2));



    /* ---------------- here we set tags (NO_SLIP, MOVING_WALL or FLUID) for all the cells ------------ */

    /* x = const walls */
    for(i = 0; i <=xlength + 1; ++i)
        for(j = 0; j <= xlength + 1; ++j)
        {
            ind = INDEX(0, i, j, xlength);
            flagField[ind] = NO_SLIP;

            ind = INDEX(xlength + 1, i, j, xlength);
            flagField[ind] = NO_SLIP;
        }

    /* y = const  walls */
    for(i = 0; i <=xlength + 1; ++i)
        for(j = 0; j <= xlength + 1; ++j)
        {
            ind = INDEX(i, 0, j, xlength);
            flagField[ind] = NO_SLIP;

            ind = INDEX(i, xlength + 1, j, xlength);
            flagField[ind] = NO_SLIP;
        }

    /* fluid in the center */
    for(i = 1; i <=xlength; ++i)
        for(j = 1; j <= xlength; ++j)
            for(k = 1; k <= xlength; ++k)
            {
                ind = INDEX(i, j, k, xlength);
                flagField[ind] = FLUID;
            }


    /* bottom  z = const */
    for(i = 0; i <=xlength + 1; ++i)
        for(j = 0; j <= xlength + 1; ++j)
        {
            ind = INDEX(i, j, 0, xlength);
            flagField[ind] = NO_SLIP;

            ind = INDEX(i, j, xlength + 1, xlength);
            flagField[ind] = MOVING_WALL;
        }
}

