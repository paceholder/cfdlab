
#include "test.h"
#include <stdio.h>
#include "computeCellValues.h"


int checkParticleDist( double* collideField, int xlength ){
    int i,j,k;
    int cIndex;
    int fIndex;
    int error = 0;
    printf( "--> Test of Particle Distribution . . . \n" ) ;

    for (i = 0; i <= xlength + 1; ++i)
    {
        for (j = 0; j <=xlength + 1; ++j)
        {
            for (k = 0 ; k <= xlength + 1; ++k)
            {
                fIndex = Q * INDEX( i, j, k, xlength );
                for ( cIndex = 0 ; cIndex < Q; ++cIndex)
                {
                    if ( collideField[ fIndex + cIndex] < 0 )
                    {
                        printf("(%d, %d, %d) - %d - FAILED!\n", i, j, k, cIndex);
                        error = 1;
                    }
                }
            }
        }

    }

    if ( error == 0)
        printf( "SUCCESSED!\n\n" );
    return 1;
}

int checkDensity( double* collideField, int xlength ){
  int i,j,k;
  int fIndex;
  double density;
  double diff;
  printf( "--> Test of density . . . " ) ;
    for (i = 0; i <= xlength + 1; ++i)
    {
        for (j = 0; j <= xlength + 1; ++j)
        {
            for (k = 0 ; k <= xlength + 1; ++k)
            {
                fIndex = Q*INDEX( i,j,k, xlength );
                computeDensity(collideField + fIndex, &density);

                diff = density - 1.0;
                if ( diff < 0 )
                diff = diff * -1;
                if( diff  > 0.3 )
                {
                    printf( "%f (%d, %d, %d)- FAILED!\n", density, i, j, k );
                }
            }
        }

    }
  printf( "SUCCESSED!\n\n" );
  return 1;

}
