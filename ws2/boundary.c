#include "boundary.h"
#include "LBDefinitions.h"
#include "computeCellValues.h"
#include <stdio.h>


/*
    function checks if we are still in computational domain

    NEEED TO BE TESTED AND CHECKED!!!!
    I'm not sure I did everything right

    it makes transformation (index) -> (i, j, k)
*/
int isCellInRange(int i, int j, int k, const int xlength)
{
    int result = -1;

    if ((i > 0 && i < xlength + 1) &&
        (j > 0 && j < xlength + 1) &&
        (k > 0 && k < xlength + 1))
        result = 0;

    return result;

    /*  0 is OK     -1 is NOT OK     */
}


void treatBoundary(double *collideField, int* flagField, const double * const wallVelocity, int xlength)
{
    int i, j, k, l;

    /* we go through all the points and process just points with NO_SLIP or MOVING_WALL flags */

    for(i = 0; i <= xlength + 1; ++i)
        for(j = 0; j <= xlength + 1; ++j)
            for(k = 0; k <= xlength + 1; ++k)
            {
                /* index of current cell */
                int index = INDEX(i, j, k, xlength);
                int index2;

                if (flagField[index] == FLUID)
                    continue;

                /* here we check all the cell around current one
                   and adjust values fi if one of surrounding cell is FLUID.
                   We change fi only if ci points to FLUID cell
                */
                for(l = 0; l < Q; ++l)
                {
                    /* offset to neighbour cell */
                    const int* const c = LATTICEVELOCITIES[l];

                    /* coordinate of neighbour cell */
                    index2 = INDEX(i + c[0], j + c[1], k + c[2], xlength);

                    /* index2 could be outside of computational domain. We process indicies just lying inside ( in FLUID zone )*/
                    if (isCellInRange(i + c[0], j + c[1], k + c[2], xlength) == 0 &&
                        flagField[index2] == FLUID)
                    {
                        /* we are in border layer */
                        if (flagField[index] == NO_SLIP)
                        {
                            /* to define the index of opposite ci we do   new_i = Q - i - 1
                            it follows from LATTICEVELOCITIES array */

                            collideField[Q * index + l] = collideField[Q * index2 + (Q - l - 1)];
                        }
                        /* another type of boundary */
                        else if (flagField[index] == MOVING_WALL)
                        {
                            double density;
                            double d;
                            double p;

                            computeDensity(collideField + Q * index2, &density);

                            p = SCALAR3(c, wallVelocity);
                            d =  2 * LATTICEWEIGHTS[l] * density * p / (C_S * C_S);
                            collideField[Q * index + l] = collideField[Q * index2 + (Q - l - 1)] + d;
                            if (collideField[Q * index + l] < 0)
                                printf("loooooser");
                        }
                    }
                }
            }
}

