#ifndef _MAIN_C_
#define _MAIN_C_

#include "collision.h"
#include "streaming.h"
#include "initLB.h"
#include "visualLB.h"
#include "boundary.h"
#include "test.h"

int main (int argc, char *argv[])
{
    double *collideField = NULL;
    double *streamField = NULL;
    int *flagField = NULL;
    int xlength;
    double tau;
    double velocityWall[3];
    int timesteps;
    int timestepsPerPlotting;
    int t;
    char* outFileName = "out.vtk";


    int res = readParameters(&xlength, &tau, velocityWall, &timesteps, &timestepsPerPlotting, argc, argv);

    /* in case of error we return code of error */
    if (res != 0)
        return res;

    allocateMemory(&collideField, &streamField, &flagField, xlength);
    initialiseFields(collideField, streamField, flagField, xlength);


    for(t = 0; t < timesteps; ++t)
    {
        double* swap = NULL;

        doStreaming(collideField, streamField, flagField, xlength);

        swap = collideField;
        collideField = streamField;
        streamField = swap;

        doCollision(collideField, flagField, &tau, xlength);


        treatBoundary(collideField, flagField, velocityWall, xlength);

        if (t%timestepsPerPlotting == 0){
            writeVtkOutput(collideField, flagField, outFileName, t, xlength);
        }
    }

  return 0;
}

#endif

