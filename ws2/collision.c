#include "collision.h"
#include "LBDefinitions.h"

void computePostCollisionDistributions(double *currentCell, const double * const tau, const double *const feq){
    int i;
    for(i=0; i<Q; i++){
        currentCell[i]=currentCell[i]-(1.0/(*tau)*(currentCell[i]-feq[i]));
    }
}

void doCollision(double *collideField, int *flagField,const double * const tau,int xlength){
    double density;
    double velocity[] = {0.0, 0.0, 0.0};
    double feq[Q];
    int i,j,k;

    for(i = 0; i <= xlength+1; i++)
        for(j = 0; j <= xlength+1; j++)
            for(k = 0; k <= xlength+1; k++)
            {
                int index = INDEX(i,j,k,xlength);

                if (flagField[index] != FLUID)
                    continue;

                computeDensity(collideField + Q * index, &density);
                computeVelocity(collideField + Q * index, &density, velocity);
                computeFeq(&density, velocity, feq);
                computePostCollisionDistributions(collideField + Q * index, tau, feq);
            }
}

