#ifndef _TEST_H_
#define _TEST_H_

#include "LBDefinitions.h"

int checkParticleDist( double* collideField, int xlength );
int checkDensity( double* collideField, int xlength );


#endif 
