#include "computeCellValues.h"
#include "LBDefinitions.h"
#include <math.h>
#include <stdio.h>

#define SCALAR3(a, b) (a[0]*b[0] + a[1]*b[1] + a[2] * b[2])

void computeDensity(const double *const currentCell, double *density){
    int i;

    *density = 0; 
    
    for (i = 0; i < Q; ++i)
        *density += currentCell[i];
}

void computeVelocity(const double * const currentCell, const double * const density, double *velocity){
    int i;
    double tmp[] = {0.0, 0.0, 0.0};

    for (i = 0; i < Q; ++i)
    {
        tmp[0] += currentCell[i] * LATTICEVELOCITIES[i][0];
        tmp[1] += currentCell[i] * LATTICEVELOCITIES[i][1];
        tmp[2] += currentCell[i] * LATTICEVELOCITIES[i][2];
    }

    velocity[0] = tmp[0]/(*density);
    velocity[1] = tmp[1]/(*density);
    velocity[2] = tmp[2]/(*density);
}

void computeFeq(const double * const density, const double * const velocity, double *feq){
    int i;
    double C_S2 = C_S * C_S;
    double C_S4 = C_S2 * C_S2;

    for (i=0; i<Q; i++)
    {
        double a = SCALAR3(velocity, LATTICEVELOCITIES[i]);
        double b = pow(SCALAR3(velocity, LATTICEVELOCITIES[i]),2);
        double c = SCALAR3(velocity, velocity);
        feq[i] = LATTICEWEIGHTS[i] * (*density) * (1.0 + a / C_S2
                                                   + b / (2 * C_S4)
                                                   - c / (2 * C_S2));
    }
}

