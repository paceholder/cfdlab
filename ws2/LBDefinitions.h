#ifndef _LBDEFINITIONS_H_
#define _LBDEFINITIONS_H_

#define Q 19
#define FLUID 0
#define NO_SLIP 1
#define MOVING_WALL 2


#define INDEX(i, j, k, len) ( (k)*(len + 2) * (len + 2) + (j)*(len + 2) +(i) )

/* scalar product of two vectors*/
#define SCALAR3(a, b) (a[0]*b[0] + a[1]*b[1] + a[2] * b[2])


static const int LATTICEVELOCITIES[19][3] = {{0, -1, -1},
                                             {-1, 0, -1},
                                             {0, 0, -1},
                                             {1, 0, -1},
                                             {0, 1, -1},
                                             {-1, -1, 0},
                                             {0, -1, 0},
                                             {1, -1, 0},
                                             {-1, 0, 0},
                                             {0, 0, 0},
                                             {1, 0, 0},
                                             {-1, 1, 0},
                                             {0, 1, 0},
                                             {1, 1, 0},
                                             {0, -1, 1},
                                             {-1, 0, 1},
                                             {0, 0, 1},
                                             {1, 0, 1},
                                             {0, 1, 1} };

static const double LATTICEWEIGHTS[19]=   {1.0/36.0, 1.0/36.0, 2.0/36.0, 1.0/36.0, 1.0/36.0,
                                            1.0/36.0, 2.0/36.0, 1.0/36.0, 2.0/36.0, 12.0/36.0,
                                            2.0/36.0, 1.0/36.0, 2.0/36.0, 1.0/36.0, 1.0/36.0,
                                            1.0/36.0, 2.0/36.0, 1.0/36.0, 1.0/36.0};

static const double C_S = 1.0 / 1.73205080; /* it is 1/sqrt(3.0)   C compiler does not like sqrt() in const definition */

#endif

