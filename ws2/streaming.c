#include "streaming.h"
#include "LBDefinitions.h"
#include "computeCellValues.h"

void doStreaming(double *collideField, double *streamField,int *flagField,int xlength){

    /* we transfer information from the collideField x + ci to the streamingField i */

    int ind1, ind2;

    int i, j, k, l;

    for(i = 1; i < xlength + 1; ++i)
        for(j = 1; j < xlength + 1; ++j)
            for(k = 1; k < xlength + 1; ++k)
            {
                ind1 = INDEX(i, j, k, xlength);

                for(l = 0; l < Q; ++l)
                {
                    double d = 0;
                    /* direction to the neighbor cell */
                    const int* const c = LATTICEVELOCITIES[l];

                    /* index of neighbor cell */
                    ind2 = INDEX(i - c[0], j - c[1], k - c[2], xlength);


                    /* x + ci are copied from the collideField to the i-th position in the streamingField. */

                    d = collideField[Q * ind2 + l];
                    streamField[Q * ind1 + l] = d;
                }
            }

        }

