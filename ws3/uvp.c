#include "uvp.h"

#include <stdlib.h> /*for abs*/
#include <math.h>
#include <assert.h>

#include "init.h"

#include "definitions.h"


void calculate_fg(
  double Re,
  double GX,
  double GY,
  double alpha,
  double dt,
  double dx,
  double dy,
  int imax,
  int jmax,
  double **U,
  double **V,
  double **F,
  double **G,
  int **FlagField
)
{
    /* indexing [row][col]
       j - row
       i - column */

    int i;
    int j;


    /* border */
    for(j = 1; j <= jmax; ++j)
    {
        F[0][j] = U[0][j];
        F[imax][j] = U[imax][j];
    }

    /* border */
    for(i = 1; i <= imax; ++i)
    {
        G[i][0] = V[i][0];
        G[i][jmax] = V[i][jmax];
    }
    /*boundries on inner cells/obstacles*/
    for (i = 1; i <= imax; i++)
    {
        for (j = 1; j <= jmax; j++)
        {
            if( (FlagField[i][j] & FLUID) == 0)
            {
                if( (FlagField[i][j] & NORTH) && (FlagField[i][j] & EAST))
                {
                    F[i][j] = U[i][j];
                    G[i][j] = V[i][j];
                }
                else if( (FlagField[i][j] & NORTH) && (FlagField[i][j] & WEST))
                {
                    F[i-1][j] = U[i-1][j];
                    G[i][j] = V[i][j];
                }
                else if( (FlagField[i][j] & SOUTH) && (FlagField[i][j] & WEST))
                {
                    F[i-1][j] = U[i-1][j];
                    G[i][j-1] = V[i][j-1];
                }
                else if((FlagField[i][j] & SOUTH) && (FlagField[i][j] & EAST))
                {
                    F[i][j] = U[i][j];
                    G[i][j-1]=  V[i][j-1];
                }
                /*Edges*/
                else if(FlagField[i][j] & NORTH)
                {
                    G[i][j]=V[i][j];
                }
                else if(FlagField[i][j] & EAST)
                {
                    F[i][j]=U[i][j];
                }
                else if(FlagField[i][j] & SOUTH)
                {
                    G[i][j-1]=V[i][j-1];
                }
                else if(FlagField[i][j] & WEST)
                {
                    F[i-1][j] = U[i-1][j];
                }
            }
        }
    }

    /* central part of the domain */
    for(i = 1; i < imax; ++i)
        for(j = 1; j < jmax + 1; ++j)
            if(FlagField[i][j] & FLUID) /*only treat fluid cells; values for boundary cells are allready set */
            {
                double d2udx2 = (U[i+1][j] - 2 * U[i][j] + U[i-1][j]) / (dx * dx);
                double d2udy2 = (U[i][j+1] - 2 * U[i][j] + U[i][j-1]) / (dy * dy);

                double du2dx = 1/dx * (pow((U[i][j] + U[i+1][j])/2.0, 2.0) - pow((U[i-1][j] + U[i][j])/2.0, 2.0)) +
                        alpha / dx * (fabs(U[i][j] + U[i+1][j])/2.0 * (U[i][j] - U[i+1][j])/2.0 - fabs(U[i-1][j] + U[i][j])/2.0 * (U[i-1][j] - U[i][j])/2.0);

                double duvdy = 1/dy * ( (V[i][j] + V[i+1][j])/2.0 * (U[i][j] + U[i][j+1])/2.0 - (V[i][j-1] + V[i+1][j-1])/2.0 * (U[i][j-1] + U[i][j])/2.0) +
                        alpha / dy * (fabs(V[i][j] + V[i+1][j])/2.0 * (U[i][j] - U[i][j+1])/2.0 - fabs(V[i][j-1] + V[i+1][j-1])/2.0 * (U[i][j-1] - U[i][j])/2.0);

                F[i][j] = U[i][j] + dt * (1/Re * (d2udx2 + d2udy2) - du2dx - duvdy + GX);

                /* assert(F[i][j] < 1000); */
            }


    for(i = 1; i < imax + 1; ++i)
        for(j = 1; j < jmax; ++j)
            if ((FlagField[i][j] & FLUID))
            {
                double d2vdx2 = (V[i+1][j] - 2 * V[i][j] + V[i-1][j]) / (dx * dx);
                double d2vdy2 = (V[i][j+1] - 2 * V[i][j] + V[i][j-1]) / (dy * dy);

                double dv2dy = 1/dy * (pow((V[i][j] + V[i][j+1])/2.0, 2.0) - pow((V[i][j-1] + V[i][j])/2.0, 2.0)) +
                        alpha / dy * (fabs(V[i][j] + V[i][j+1])/2.0 * (V[i][j] - V[i][j+1])/2.0 - fabs(V[i][j-1] + V[i][j])/2.0 * (V[i][j-1] - V[i][j])/2.0);

                double duvdx = 1/dx * ( (U[i][j] + U[i][j+1])/2.0 * (V[i][j] + V[i+1][j])/2.0 - (U[i-1][j] + U[i-1][j+1])/2.0 * (V[i-1][j] + V[i][j])/2.0) +
                        alpha / dx * (fabs(U[i][j] + U[i][j+1])/2.0 * (V[i][j] - V[i+1][j])/2.0 - fabs(U[i-1][j] + U[i-1][j+1])/2.0 * (V[i-1][j] - V[i][j])/2.0);

                G[i][j] = V[i][j] + dt * (1/Re * (d2vdx2 + d2vdy2) - duvdx - dv2dy + GY);
                /* assert(G[i][j] < 1000); */
            }
}



void calculate_rs(double dt, double dx, double dy, int imax, int jmax, double **F, double **G, double **RS)
{
    int i,j;
    for(i=1;i<imax+1;i++)
    {
        for(j=1;j<jmax+1; j++)
        {
            /* perhaps a problem that the boundary is not set for RS (only from 1 to jmax and not from 0 to jmax+1; depends on how we need it and how its used*/
            RS[i][j] = ( (F[i][j]-F[i-1][j])/dx + (G[i][j]-G[i][j-1])/dy ) / dt;
        }
    }
}



void calculate_dt(int **FlagField, Parameters* params, double tau, double *dt, double **U, double **V)
{
    int i,j;
    double umax=0;
    double vmax=0;

    double tmp;
    double min3;

    /*need to set umax and vmax*/
    for(i = 0; i <= params->imax; ++i)
    {
        /*check if its < or <= and if it should be imax+1 or something else*/
        for(j = 1; j <= params->jmax; ++j)
        {
            if (FlagField[i][j] & FLUID)
                if ( fabs(U[i][j]) > umax )
                    umax = fabs(U[i][j]);
        }
    }

    for(i = 1; i <= params->imax; ++i)
    {
        /*check if its < or <= and if it should be imax+1 or something else*/
        for(j = 0; j <= params->jmax; ++j)
        {
            if (FlagField[i][j] & FLUID)
                if( fabs(V[i][j]) > vmax)
                    vmax = fabs(V[i][j]);
        }
    }


    /*not possible to have a min with 3 inputs so min of 2 and then min of min*/
    tmp = fmin((0.5 * params->Re/(1/(params->dx*params->dx)+1/(params->dy*params->dy))), (params->dx/umax));

    min3 = fmin((params->dy/vmax), tmp);
    *dt = tau * min3;
}

/**
 * Calculates the new velocity values according to the formula
 *
 * @f$ u_{i,j}^{(n+1)}  =  F_{i,j}^{(n)} - \frac{\delta t}{\delta x} (p_{i+1,j}^{(n+1)} - p_{i,j}^{(n+1)}) @f$
 * @f$ v_{i,j}^{(n+1)}  =  G_{i,j}^{(n)} - \frac{\delta t}{\delta y} (p_{i,j+1}^{(n+1)} - p_{i,j}^{(n+1)}) @f$
 *
 * As always the index range is
 *
 * @f$ i=1,\ldots,imax-1, \quad j=1,\ldots,jmax @f$
 * @f$ i=1,\ldots,imax, \quad j=1,\ldots,jmax-1 @f$
 *
 * @image html calculate_uv.jpg
 * @author Arash Bakhtiari
 */
void calculate_uv(
  Parameters *params,
  int ** FlagField,
  double dt,
  double **U,
  double **V,
  double **F,
  double **G,
  double **P
)
{
  int i;
  int j;

    /* Computing the U at the next time step */
    for ( i = 1 ; i <= params->imax-1 ; i++)
    {
        for ( j = 1; j <= params->jmax ; j++ )
        {
            U[i][j] = 0;
            if (FlagField[i][j] & FLUID)
                U[i][j] = F[i][j] - (dt/params->dx)*( P[i+1][j] - P[i][j] ) ;

        }
    }

    /* Computing the V at the next time step */
    for ( i = 1 ; i <= params->imax ; i++)
    {
        for ( j = 1; j <= params->jmax-1 ; j++ )
        {
            V[i][j] = 0;
            if (FlagField[i][j] & FLUID)
                V[i][j] = G[i][j] - (dt/params->dy)*( P[i][j+1] - P[i][j] ) ;
        }
    }
}
