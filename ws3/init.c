#include "helper.h"
#include "init.h"
#include "definitions.h"

/* need to put this into common file */
int read_parameters( const char *szFileName,       /* name of the file */
                    double *UI,
                    double *VI,                /* velocity x-direction */
                    double *PI,                /* velocity y-direction */
                    double *GX,                /* pressure */
                    double *GY,                /* gravitation x-direction */
                    double *t_end,                /* gravitation y-direction */
                    double *dt,           /* length of the domain y-dir.*/
                    double *alpha,                /* number of cells y-direction*/
                    double *omg,             /* uppwind differencing factor*/
                    double *tau,               /* relaxation factor */
                    int *itermax,               /* safety factor for time step*/
                    double *eps,             /* max. number of iterations  */
                                               /* for pressure per time step */
                    double *dt_value,               /* accuracy bound for pressure*/
                    char *mapFileName,          /* time for output */
                    Parameters *params
                    )
{
    double Re;
    double pressure_left;
    double pressure_right;
    double wall_speed;
    double u_inflow;
    double v_inflow;
    double xlength;
    double ylength;

    READ_DOUBLE( szFileName, pressure_left);
    READ_DOUBLE( szFileName, pressure_right);
    READ_DOUBLE( szFileName, wall_speed);
    READ_DOUBLE( szFileName, u_inflow);
    READ_DOUBLE( szFileName, v_inflow);
    READ_DOUBLE( szFileName, Re    );
    READ_DOUBLE( szFileName, xlength );
    READ_DOUBLE( szFileName, ylength );

    params->Re = Re;
    params->pressure_left = pressure_left;
    params->pressure_right = pressure_right;
    params->wall_speed = wall_speed;
    params->u_inflow = u_inflow;
    params->v_inflow = v_inflow;
    params->xlength = xlength;
    params->ylength = ylength;


    READ_DOUBLE( szFileName, *t_end );
    READ_DOUBLE( szFileName, *dt    );

    READ_DOUBLE( szFileName, *omg   );
    READ_DOUBLE( szFileName, *eps   );
    READ_DOUBLE( szFileName, *tau   );
    READ_DOUBLE( szFileName, *alpha );

    READ_INT   ( szFileName, *itermax );
    READ_DOUBLE( szFileName, *dt_value );

    READ_DOUBLE( szFileName, *UI );
    READ_DOUBLE( szFileName, *VI );
    READ_DOUBLE( szFileName, *GX );
    READ_DOUBLE( szFileName, *GY );
    READ_DOUBLE( szFileName, *PI );

    READ_STRING( szFileName, mapFileName);

   /* reading parameters of experiment */



   return 1;
}


void init_uvp(
    double UI,
    double VI,
    double PI,
    int imax,
    int jmax,
    double **U,
    double **V,
    double **P)
{
    init_matrix(P, 0, imax, 0, jmax, PI);
    init_matrix(U, 0, imax, 0, jmax, UI);
    init_matrix(V, 0, imax, 0, jmax, VI);
}

void check_flag(Parameters* params, int** Flagfield,int *status)
{
    int i,j;

    for(i = 1; i <= params->imax; i++)
    {
        for(j = 1;j <= params->jmax; j++)
        {
            /* initialisation of flagfield complete */
            /* checking for forbidden cells */

            /* on boundary cells */
            if( (Flagfield[i][j] & FLUID) == 0) /* this cell is not fluid */
            {
                int a = (Flagfield[i][j] & EAST) ? 1 : 0;
                int b = (Flagfield[i][j] & WEST) ? 1 : 0;
                int c = (Flagfield[i][j] & NORTH) ? 1 : 0;
                int d = (Flagfield[i][j] & SOUTH) ? 1 : 0;
                if (  a + b + c + d > 2 )
                {
                    *status = -1;
                    return;
                }
            }
            else /* this cell is fluid. it can not have mor than 2 obstacle cells around */
            {
                int a = (Flagfield[i+1][j] & FLUID) ? 0 : 1; /* is neighbor obstacle? */
                int b = (Flagfield[i-1][j] & FLUID) ? 0 : 1;
                int c = (Flagfield[i][j+1] & FLUID) ? 0 : 1;
                int d = (Flagfield[i][j-1] & FLUID) ? 0 : 1;
                if (  a + b + c + d > 2 )
                {
                    *status = -1;
                    return;
                }
            }

            /*      |_|_|_|
             *      |||?|||
             *      |_|_|_|
             */
            if ((Flagfield[i+1][j] & FLUID) == (Flagfield[i-1][j] & FLUID) &&
                (Flagfield[i][j+1] & FLUID) == (Flagfield[i][j-1] & FLUID) &&
                (Flagfield[i+1][j] & FLUID) != (Flagfield[i][j+1] & FLUID))
            {
                *status = -1;
                return;
            }
        }
    }
}
