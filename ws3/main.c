#include "helper.h"
#include "visual.h"
#include "init.h"
#include "uvp.h"
#include "sor.h"
#include "boundary_val.h"
#include "helper.h"
#include "definitions.h"

#include <stdio.h>


/**
 * The main operation reads the configuration file, initializes the scenario and
 * contains the main loop. So here are the individual steps of the algorithm:
 *
 * - read the program configuration file using read_parameters()
 * - set up the matrices (arrays) needed using the matrix() command
 * - create the initial setup init_uvp(), init_flag(), output_uvp()
 * - perform the main loop
 * - trailer: destroy memory allocated and do some statistics
 *
 * The layout of the grid is decribed by the first figure below, the enumeration
 * of the whole grid is given by the second figure. All the unknowns corresond
 * to a two dimensional degree of freedom layout, so they are not stored in
 * arrays, but in a matrix.
 *
 * @image html grid.jpg
 *
 * @image html whole-grid.jpg
 *
 * Within the main loop the following big steps are done (for some of the
 * operations a definition is defined already within uvp.h):
 *
 * - calculate_dt() Determine the maximal time step size.
 * - boundaryvalues() Set the boundary values for the next time step.
 * - calculate_fg() Determine the values of F and G (diffusion and confection).
 *   This is the right hand side of the pressure equation and used later on for
 *   the time step transition.
 * - calculate_rs()
 * - Iterate the pressure poisson equation until the residual becomes smaller
 *   than eps or the maximal number of iterations is performed. Within the
 *   iteration loop the operation sor() is used.
 * - calculate_uv() Calculate the velocity at the next time step.
 */
int main(int argn, char** args)
{
    const char* outputFileName = "out.vtk";
    double UI, VI, PI, GX, GY;
    double t_end, dt;
    int itermax;
    int status;
    double alpha, omg, tau;
    double eps, dt_value;
    char mapFileName[500];

    Parameters params;

    int** FlagField;
    double** U;
    double** V;
    double** F;
    double** G;
    double** RS;
    double** P;

    double t = 0.0;
    int n = 0;

    if (argn < 2)
    {
        printf("Please pass parameters file as an argument");
        return 0;
    }




    /* first read parameters ... */
    read_parameters(args[1], &UI, &VI, &PI, &GX, &GY, &t_end,
                    &dt,
                    &alpha, &omg, &tau, &itermax, &eps, &dt_value,
                    mapFileName,
                    &params);

    /* read our map which sets geometry and obstacles */
    FlagField = read_ppm(mapFileName, &params);

    check_flag(&params, FlagField, &status);

    if ( status == -1 )
    {
        printf("Error in Your scenario file; please check geometry again");
        return status;
    }

    /* allocation of the space  */
    U  = matrix(0, params.imax + 1, 0, params.jmax + 1);
    V  = matrix(0, params.imax + 1, 0, params.jmax + 1);
    F  = matrix(0, params.imax + 1, 0, params.jmax + 1);
    G  = matrix(0, params.imax + 1, 0, params.jmax + 1);
    RS = matrix(0, params.imax + 1, 0, params.jmax + 1);
    P  = matrix(0, params.imax + 1, 0, params.jmax + 1);

    /* assign initial values */
    init_uvp(UI, VI, PI, params.imax, params.jmax, U, V, P);

    while (t < t_end)
    {
        int it = 0;
        double res = 1.0;

        if (n % 4 == 0)
          write_vtkFile(outputFileName, n, params.xlength, params.ylength, params.imax, params.jmax, params.dx, params.dy, U, V, P);

        /* new value of dt */
        calculate_dt(FlagField, &params, tau, &dt, U , V);

        boundaryvalues(U, V, FlagField, &params);

        calculate_fg(params.Re, GX, GY, alpha, dt, params.dx, params.dy, params.imax, params.jmax, U, V, F, G, FlagField);

        calculate_rs(dt, params.dx, params.dy, params.imax, params.jmax, F, G, RS);

        /* sor iteration */
        while (it < itermax && res > eps)
        {
            sor(omg, params.dx, params.dy, params.imax, params.jmax, P, RS, &res, FlagField, &params);
            ++it;
        }

        calculate_uv(&params, FlagField, dt, U, V, F, G, P);

        t += dt;
        ++n;
    }


    /* clean up memory */

    free_matrix(P, 0, params.imax+1, 0, params.jmax+1);
    free_matrix(RS, 0, params.imax+1, 0, params.jmax+1);
    free_matrix(G, 0, params.imax+1, 0, params.jmax+1);
    free_matrix(F, 0, params.imax+1, 0, params.jmax+1);
    free_matrix(V, 0, params.imax+1, 0, params.jmax+1);
    free_matrix(U, 0, params.imax+1, 0, params.jmax+1);


    return 0;
}
