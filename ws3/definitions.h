#ifndef __DEFINITIONS_H__
#define __DEFINITIONS_H__

/* this macro computes the y or x position of element i with grid size dx */
#define COM_LENGTH(i,dx) ( ( (i) - 0.5 ) *  (dx) )

#define FLUID 16   /* 10000 */
#define EAST   8   /* 01000 */
#define WEST   4   /* 00100 */
#define SOUTH  2   /* 00010 */
#define NORTH  1   /* 00001 */

#define NW (NORTH | WEST)
#define NE (NORTH | EAST)
#define SW (SOUTH | WEST)
#define SE (SOUTH | EAST)

/* pressure flags */
#define PRESSURE_LEFT (64 | OUTFLOW)
#define PRESSURE_RIGHT (32 | OUTFLOW)

#define NOSLIP   (1 << 7)  /* 00001 00 00000 */
#define FREESLIP (2 << 7)  /* 00010 00 00000 */
#define OUTFLOW  (4 << 7)  /* 00100 00 00000 */

/* inflow flag */
#define INFLOW_CONST_VELOCITY ( 8 << 7 )  /* 01000 00 00000 */
#define INFLOW_SHEAR_VELOCITY ( 16 << 7 ) /* 10000 00 00000 */


typedef struct
{
    double Re;
    double pressure_left;
    double pressure_right;
    double wall_speed;
    double u_inflow;
    double v_inflow;
    double xlength;
    double ylength;
    int imax;
    int jmax;
    double dx;
    double dy;
} Parameters;

#endif
