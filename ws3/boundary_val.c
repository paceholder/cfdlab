#include "boundary_val.h"
#include "definitions.h"

/**
 * The boundary values of the problem are set.
 */
void boundaryvalues(
    double **U,
    double **V,
    int **FlagField,
    Parameters* params
)
{
    int j;
    int i;

        /*left and right outter Boundary*/
    for (  j = 1 ; j <= params->jmax ; ++j)
    {
        /*left*/
        int k = 0;

        if( FlagField[k][j] & NOSLIP)
        {
            U[k][j] = 0 ;
            V[k][j] = -V[k+1][j];
        }

        else if(FlagField[k][j] & OUTFLOW )
        {
            U[k][j] = U[k+1][j] ;
            V[k][j] = V[k+1][j];
        }
        else if(FlagField[k][j] & FREESLIP)
        {
            U[k][j] = 0;
            V[k][j] = V[k+1][j];
        }

        /*right*/
        k = params->imax + 1;

        if ( FlagField[k][j] & NOSLIP)
        {
            U[k][j] = 0 ;
            V[k][j] = -V[k-1][j];
        }

        else if( FlagField[k][j] & OUTFLOW )
        {
            U[k-1][j] = U[k-2][j];
            V[k][j] = V[k-1][j];
        }

        else if( FlagField[k][j] & FREESLIP )
        {
            U[k][j] = 0;
            V[k][j] = V[k-1][j];
        }
    }

    /*BOTTOM AND TOP OUTER BOUNDARIES*/

    for(  i = 1; i <= params->imax ; i++ )
    {
        /*BOTTOM*/
        int l= 0;

        if( FlagField[i][l] & NOSLIP )
        {
            U[i][l] = -U[i][l+1];
            V[i][l] = 0;
        }

        else if( FlagField[i][l] & OUTFLOW )
        {
            U[i][l] = U[i][l+1];
            V[i][l] = V[i][l+1];
        }

        else if( FlagField[i][l] & FREESLIP )
        {
            U[i][l] = U[i][l+1];
            V[i][l] = 0;
        }

        /*TOP*/
        l = params->jmax+1;

        if( FlagField[i][l] & NOSLIP )
        {
            U[i][l] = -U[i][l-1];
            V[i][l] = 0;
        }

        else if( FlagField[i][l] & OUTFLOW)
        {
            U[i][l] = U[i][l-1];
            V[i][l-1] = V[i][l-2];
        }

        else if( FlagField[i][l] & FREESLIP)
        {
            U[i][l] = U[i][l-1];
            V[i][l] = 0;
        }
    }

    /*treating outer boundaries for special cases*/
    spec_boundary_val( U, V, FlagField, params );

    /* boundries on inner cells/obstacles */
    for (i = 1; i <= params->imax; i++)
    {
        for (j = 1; j <= params->jmax; j++)
        {
            if( (FlagField[i][j] & FLUID) == 0)
            {
                /*corners*/

                /* simple FF[i][j] & NE  > 0 will not work
                 * since it greater than zero even if just one bit is 1
                 */
                if( (FlagField[i][j] & NORTH) && (FlagField[i][j] & EAST))
                {
                    /*     _____x_____
                     *    |           |
                     *    |           |
                     *    |           x
                     *    |           |
                     *    |___________|
                     */
                    U[i][j] = 0;
                    V[i][j] = 0;

                    U[i-1][j] = - U[i-1][j+1];
                    V[i][j-1] = - V[i+1][j-1];
                }
                else if( (FlagField[i][j] & NORTH) && (FlagField[i][j] & WEST))
                {
                    /*     _____x_____
                     *    |           |
                     *    |           |
                     *    x           |
                     *    |           |
                     *    |___________|
                     */
                    U[i-1][j] = 0;
                    V[i][j] = 0;

                    U[i][j] = - U[i][j+1];
                    V[i][j-1] = - V[i-1][j-1];
                }
                else if( (FlagField[i][j] & SOUTH ) && (FlagField[i][j] & WEST))
                {
                    /*     ___________
                     *    |           |
                     *    |           |
                     *    x           |
                     *    |           |
                     *    |_____x_____|
                     */
                    U[i-1][j]=0;
                    V[i][j-1]=0;

                    U[i][j] = - U[i][j-1];
                    V[i][j] = - V[i-1][j];
                }
                else if((FlagField[i][j] & SOUTH) && (FlagField[i][j] & EAST))
                {
                    /*     ___________
                     *    |           |
                     *    |           |
                     *    |           x
                     *    |           |
                     *    |_____x_____|
                     */

                    U[i][j] = 0;
                    V[i][j-1] = 0;

                    U[i-1][j] = - U[i-1][j-1];
                    V[i][j] = - V[i+1][j];
                }

                /* Edges */
                else if(FlagField[i][j] & NORTH)
                {
                    U[i][j] = - U[i][j+1];
                    V[i][j] = 0;
                }
                else if(FlagField[i][j] & EAST)
                {
                    U[i][j] = 0;
                    V[i][j] = - V[i+1][j];
                }
                else if(FlagField[i][j] & SOUTH)
                {
                    U[i][j] = - U[i][j-1];
                    V[i][j-1] = 0;
                }
                else if(FlagField[i][j] & WEST)
                {
                    V[i][j] = - V[i-1][j];
                    U[i-1][j] = 0;
                }
            }
        }
    }
}


void spec_boundary_val( double **U, double **V, int** FlagField, Parameters *parameters )
{
    /* now I set velocity just for left wall */
    double constUVelocity = parameters->u_inflow;
    double constVVelocity = parameters->v_inflow;
    double dP = parameters->pressure_left;
    double dX = parameters->dx;
    double dY = parameters->dy;
    double height = parameters->ylength;
    double Re = parameters->Re;
    int j;

    /* left and right walls */
    for(j = 1; j <= parameters->jmax; j++)
    {
        /* left wall */
        /* const veloctiy */
        if( (FlagField[0][j] & INFLOW_CONST_VELOCITY) > 0 )
        {
            U[0][j] = constUVelocity;
            V[0][j] = 2 * constVVelocity - V[1][j];
        }
        /* shear velocity */
        /*
        else if( (FlagField[0][j] & INFLOW_SHEAR_VELOCITY) > 0 )
        {
            double len = COM_LENGTH(j, dY);
            U[0][j] = -0.5 * Re * dP / dX * len * ( len - height ) ;
            V[0][j] = - V[1][j];
        }
        */

        /* right wall */
        /* const veloctiy */
        if( (FlagField[parameters->imax+1][j] & INFLOW_CONST_VELOCITY) > 0 )
        {
            U[parameters->imax+1][j] = constUVelocity;
            V[parameters->imax+1][j] = 2 * constVVelocity - V[parameters->imax][j];
        }

        /* shear velocity */
        else if( (FlagField[parameters->imax+1][j] & INFLOW_SHEAR_VELOCITY) > 0 )
        {
            U[parameters->imax+1][j] = -0.5 * Re * dP / dX * COM_LENGTH(j, dY) * ( COM_LENGTH(j, dY) - height) ;;
            V[parameters->imax+1][j] = - V[parameters->imax][j];
        }
    }
}
