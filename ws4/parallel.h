#ifndef _PARALLEL_H_
#define _PARALLEL_H_

#include "mpi.h"
#include "init.h"
#include "parameters.h"
#include "procstate.h"


#include <stdlib.h>
#include <stdio.h>



void Program_Message(char *txt);
/* produces a stderr text output  */



void Programm_Sync(char *txt);
/* produces a stderr textoutput and synchronize all processes */



void Programm_Stop(char *txt);
/* all processes will produce a text output, be synchronized and finished */


/* initialisation of the parameters for current subdomain/process */
void init_parallel(Parameters* parameters, ProcState* procState);

void pressure_comm(double **P,
		   ProcState* procState,
                   double *bufSend,
                   double *bufRecv,int chunk
                   );
void uv_comm(double **U,
	     double **V,
	     ProcState* procState,
	     double *bufSend,
	     double *bufRecv,
	     int chunk
	     );

#endif
