#ifndef __RANDWERTE_H__
#define __RANDWERTE_H__

#include "parallel.h"

/**
 * The boundary values of the problem are set.
 */
void boundaryvalues(double **U, double **V, ProcState* procState);

#endif
