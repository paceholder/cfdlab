#include "sor.h"

#include <math.h>
#include <mpi.h>


void sor(
    Parameters* parameters,
    double **P,
    double **RS,
    double *res,
    ProcState* procState,
    double *bufSend,
    double *bufRecv,
    int chunk
)
{
    int i,j;
    double rloc;
    double sum = 0;
    double omg = parameters->omg;
    double dx = parameters->dx;
    double dy = parameters->dy;

    double coeff = omg/(2.0*(1.0/(dx*dx)+1.0/(dy*dy)));

    /* SOR iteration */
    for(i = 1; i <= procState->ir - procState->il + 1; ++i)
    {
        for(j = 1; j <= procState->jt - procState->jb + 1; ++j)
        {
            P[i][j] = (1.0 - omg) * P[i][j]
                          + coeff * (( P[i+1][j] + P[i-1][j]) / (dx*dx) + ( P[i][j+1] + P[i][j-1]) / (dy*dy) - RS[i][j]);
        }
    }

    pressure_comm(P,  procState, bufSend,  bufRecv, chunk  );

    /* compute the residual */
    rloc = 0;
    for(i = 1; i <= procState->ir - procState->il + 1; ++i)
    {
        for(j = 1; j <= procState->jt - procState->jb + 1; ++j)
        {
            rloc += ( (P[i+1][j ] -2.0 * P[i][j] + P[i-1][j])/(dx*dx) + ( P[i][j+1] - 2.0 * P[i][j] + P[i][j-1]) / (dy*dy) - RS[i][j]) *
                    ( (P[i+1][j] - 2.0 * P[i][j] + P[i-1][j])/(dx*dx) + ( P[i][j+1] - 2.0 * P[i][j] + P[i][j-1]) / (dy*dy) - RS[i][j]);
        }
    }
    /*rloc = rloc/((procState->ir - procState->il + 1) * (procState->jt - procState->jb + 1));*/
    /* set residual */
    /**res = rloc;*/

    /* here we add all rloc and save the sum in res */
    MPI_Reduce(&rloc, &sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if ( procState->myrank == 0 )
      {
	*res = sqrt(sum/(parameters->imax*parameters->jmax));
	/* sprintf(msg,"residual: %f",*res); */
	/* Program_Message(msg); */
      }

    /* then we distribute global error among all the processes */
    MPI_Bcast(res, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);


    /* set boundary values for pressure */
    if (procState->rank_b == MPI_PROC_NULL) 
        for(i = 1; i <= procState->ir - procState->il + 1; ++i) 
        {
            P[i][0] = P[i][1];
        }

    if (procState->rank_t == MPI_PROC_NULL) 
        for(i = 1; i <= procState->ir - procState->il + 1; ++i) 
        {
            P[i][procState->jt - procState->jb + 2] = P[i][procState->jt - procState->jb + 1];
        }


    if (procState->rank_l == MPI_PROC_NULL) 
        for(j = 0; j <= procState->jt - procState->jb + 1; ++j)
        {
            P[0][j] = P[1][j];
        }

    if (procState->rank_r == MPI_PROC_NULL) 
        for(j = 0; j <= procState->jt - procState->jb + 1; ++j)
        {
            P[procState->ir - procState->il + 2][j] = P[procState->ir - procState->il + 1][j];
        }

}

