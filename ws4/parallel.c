#include "parallel.h"

#include <mpi.h>
#include "init.h"

void Program_Message(char *txt)
/* produces a stderr text output  */

{
   int myrank;

   MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
   fprintf(stderr,"-MESSAGE- P:%2d : %s\n",myrank,txt);
   fflush(stdout);
   fflush(stderr);
}


void Programm_Sync(char *txt)
/* produces a stderr textoutput and synchronize all processes */

{
   int myrank;

   MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
   MPI_Barrier(MPI_COMM_WORLD);                             /* synchronize output */  
   fprintf(stderr,"-MESSAGE- P:%2d : %s\n",myrank,txt);
   fflush(stdout);
   fflush(stderr);
   MPI_Barrier(MPI_COMM_WORLD);
}


void Programm_Stop(char *txt)
/* all processes will produce a text output, be synchronized and finished */

{
   int myrank;

   MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
   MPI_Barrier(MPI_COMM_WORLD);                           /* synchronize output */
   fprintf(stderr,"-STOP- P:%2d : %s\n",myrank,txt);
   fflush(stdout);
   fflush(stderr);
   MPI_Barrier(MPI_COMM_WORLD);
   MPI_Finalize();
   exit(1);
}


#define RANK(i, j, iproc, jproc) ((j) * (iproc) + (i))

void init_parallel(Parameters* parameters, ProcState* procState)
{
    int iproc = parameters->iproc;
    int jproc = parameters->jproc;
    int imax = parameters->imax;
    int jmax = parameters->jmax;
    int rank;
    int size;

    MPI_Status status;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);


    if (size != iproc * jproc)
        Programm_Stop("Number of cells does not equal to number of processes");

    /* main process distributes the job */
    if (rank == 0)
    {
        int i, j;
        int nx = imax / iproc;
        int ny = jmax / jproc;
        ProcState state;

        /* cycle through all the subdomains */
        for(j = 0; j < jproc; ++j)
        {
            for(i = 0; i < iproc; ++i)
            {
                /* coordinates of subdomain in global domain terms */
                state.il = i * nx;
                state.ir = (i + 1) * nx - 1;

                state.jb = j * ny;
                state.jt = (j + 1) * ny - 1; 
    
                /* last cell in a row/column 
                 * explicіt assign imax/jmax
                 * since xn * iproc does not necessary equals imax
                 */
                if (i == iproc - 1)
                    state.ir = imax - 1;

                if (j == jproc - 1)
                    state.jt = jmax - 1;

                state.omg_i = i;
                state.omg_j = j;

                /* ranks of neighbors */
                state.rank_l = RANK(i-1, j, iproc, jproc);
                state.rank_r = RANK(i+1, j, iproc, jproc);
                state.rank_b = RANK(i, j-1, iproc, jproc);
                state.rank_t = RANK(i, j+1, iproc, jproc);

                /* if subdomain is near the border */
                if (i == 0)
                    state.rank_l = MPI_PROC_NULL;
                if (i == iproc - 1)
                    state.rank_r = MPI_PROC_NULL;
                if (j == 0)
                    state.rank_b = MPI_PROC_NULL;
                if (j == jproc - 1)
                    state.rank_t = MPI_PROC_NULL;

                MPI_Send(&state, sizeof(ProcState), MPI_BYTE, RANK(i, j, iproc, jproc), 10, MPI_COMM_WORLD);
            }
        }
    }
    
    MPI_Recv(procState, sizeof(ProcState), MPI_BYTE, 0, 10, MPI_COMM_WORLD, &status);
    
    procState->num_proc = size;
    procState->myrank = rank;
}

void pressure_comm(double **P,
		   ProcState* procState,
                   double *bufSend,
                   double *bufRecv,int chunk
                   )
{
  int il = procState->il;
  int ir = procState->ir;
  int jb = procState->jb;
  int jt = procState->jt;
  int rank_l = procState->rank_l;
  int rank_r = procState->rank_r;
  int rank_b = procState->rank_b;
  int rank_t = procState->rank_t;
  MPI_Status stat;
  MPI_Status* status = &stat;
  int i, j;
  int height, width;
    
  height = jt-jb+1;
  width = ir-il+1;

  /*****************************************/
  /* send to left receive from right       */
  /*****************************************/
  if(rank_l != MPI_PROC_NULL) 
    {
      for(j=0; j < height+1; j++)
        {
	  bufSend[j] = P[1][j];
        }
    }

  MPI_Sendrecv(bufSend, height+1, MPI_DOUBLE, rank_l, chunk, bufRecv, height+1, MPI_DOUBLE, rank_r, chunk, MPI_COMM_WORLD, status);

  if(rank_r != MPI_PROC_NULL) 
    {
      for(j=0; j < height+1; j++)
        {
	  P[width+1][j] = bufRecv[j];
        }
    }
    

  /*****************************************/
  /* send to right receive from left       */
  /*****************************************/
  if(rank_r != MPI_PROC_NULL)
    {
      for(j=0; j < height+1; j++)
        {
	  bufSend[j] = P[width][j];
        }
    }
  MPI_Sendrecv(bufSend, height+1, MPI_DOUBLE, rank_r, chunk, bufRecv, height+1, MPI_DOUBLE, rank_l, chunk, MPI_COMM_WORLD, status);
  if(rank_l != MPI_PROC_NULL){
    for(j=0; j < height+1; j++)
      {
	P[0][j] = bufRecv[j];
      }
  }
    

  /*****************************************/
  /* send to top receive from bottom       */
  /*****************************************/
  if(rank_t != MPI_PROC_NULL)
    {
      for(i=0; i < width+1; i++)
        {
	  bufSend[i] = P[i][height];
        }
    }

  MPI_Sendrecv(bufSend, width+1, MPI_DOUBLE, rank_t, chunk, bufRecv, width+1, MPI_DOUBLE, rank_b, chunk, MPI_COMM_WORLD, status);

  if(rank_b != MPI_PROC_NULL)
    {
      for(i=0; i < width+1; i++)
        {
	  P[i][0] = bufRecv[i];
        }
    }
    

  /*****************************************/
  /* send to bottom receive from top       */
  /*****************************************/
  if(rank_b != MPI_PROC_NULL)
    {
      for(i=0; i < width+1; i++)
        {
	  bufSend[i] = P[i][1];
        }
    }

  MPI_Sendrecv(bufSend, width+1, MPI_DOUBLE, rank_b, chunk, bufRecv, width+1, MPI_DOUBLE, rank_t, chunk, MPI_COMM_WORLD, status);

  if(rank_t != MPI_PROC_NULL)
    {
      for(i=0; i < width+1; i++)
        {
	  P[i][height+1] = bufRecv[i];
        }
    }

}

void uv_comm(double **U,
	     double **V,
	     ProcState* procState,
	     double *bufSend,
	     double *bufRecv,int chunk
	     )
{
  int il = procState->il;
  int ir = procState->ir;
  int jb = procState->jb;
  int jt = procState->jt;
  int rank_l = procState->rank_l;
  int rank_r = procState->rank_r;
  int rank_b = procState->rank_b;
  int rank_t = procState->rank_t;
  MPI_Status stat;
  MPI_Status* status = &stat;
  int imax = ir - il + 1;
  int jmax = jt - jb + 1;
  int i = 0;
  int j = 0;


  /*****************************************/
  /* send to left receive from right       */
  /*****************************************/
  
  if (rank_l != MPI_PROC_NULL){
    for (j = 1; j <= jmax; j++)
      {
	bufSend[j-1] = U[2][j];
      }
  }

  MPI_Sendrecv(bufSend, jmax, MPI_DOUBLE, rank_l, chunk, bufRecv, jmax, MPI_DOUBLE, rank_r, chunk, MPI_COMM_WORLD, status);

  if (rank_r != MPI_PROC_NULL){
    for (j = 1; j <= jmax; j++){
      U[imax+2][j]=bufRecv[j-1];
    }
  }

  
  /*****************************************/
  /* send to right receive from left       */
  /*****************************************/
  if (rank_r != MPI_PROC_NULL){
    for (j = 1; j <= jmax; j++)
      {
	bufSend[j-1] = U[imax][j];
      }
  }

  MPI_Sendrecv(bufSend, jmax, MPI_DOUBLE, rank_r, chunk, bufRecv, jmax, MPI_DOUBLE, rank_l, chunk, MPI_COMM_WORLD, status);

  if (rank_l !=MPI_PROC_NULL){
    for (j= 1;j <= jmax;j++){
      U[0][j]=bufRecv[j-1];
    }
  }


  /*****************************************/
  /* send to top receive from bottom       */
  /*****************************************/
  if (rank_t != MPI_PROC_NULL){
    for (i = 1; i <= imax + 1; i++)
      {
	bufSend[i-1] = U[i][jmax];
      }
  }

  MPI_Sendrecv(bufSend, imax+1, MPI_DOUBLE, rank_t, chunk, bufRecv, imax+1, MPI_DOUBLE, rank_b, chunk, MPI_COMM_WORLD, status);

  if (rank_b !=MPI_PROC_NULL){
    for (i = 1; i <= imax+1; i++){
      U[i][0]=bufRecv[i-1];
    }
  }

  

  /*****************************************/
  /* send to bottom receive from top       */
  /*****************************************/
  if (rank_b != MPI_PROC_NULL){
    for (i = 1; i <= imax+1; i++)
      {
	bufSend[i-1] = U[i][1];
      }
  }

  MPI_Sendrecv(bufSend, imax+1, MPI_DOUBLE, rank_b, chunk, bufRecv, imax+1, MPI_DOUBLE, rank_t, chunk, MPI_COMM_WORLD, status);

  if (rank_t !=MPI_PROC_NULL){
    for (i = 1; i <= imax+1;i++){
      U[i][jmax+1]=bufRecv[i-1];
    }
  }


  /*****************************************/
  /* send to left receive from right       */
  /*****************************************/
  if (rank_l != MPI_PROC_NULL){
    for (j = 1; j <= jmax + 1; j++)
      {
	bufSend[j-1] = V[1][j];
      }
  }

  MPI_Sendrecv(bufSend, jmax+1, MPI_DOUBLE, rank_l, chunk, bufRecv, jmax+1, MPI_DOUBLE, rank_r, chunk, MPI_COMM_WORLD, status);

  if (rank_r !=MPI_PROC_NULL){
    for (j = 1; j <= jmax + 1; j++){
      V[imax+1][j]=bufRecv[j-1];
    }
  }
    
  
  /*****************************************/
  /* send to right receive from left       */
  /*****************************************/
  if (rank_r != MPI_PROC_NULL){
    for (j = 1; j <= jmax + 1; j++)
      {
	bufSend[j-1] = V[imax][j];
      }
  }

  MPI_Sendrecv(bufSend, jmax+1, MPI_DOUBLE, rank_r, chunk, bufRecv, jmax+1, MPI_DOUBLE, rank_l, chunk, MPI_COMM_WORLD, status);

  if (rank_l !=MPI_PROC_NULL){
    for (j = 1 ; j <= jmax + 1; j++){
      V[0][j] = bufRecv[j-1];
    }
  }

  /*****************************************/
  /* send to top receive from bottom      */
  /*****************************************/
  
  if (rank_t != MPI_PROC_NULL){
    for (i = 1; i <= imax; i++)
      {
	bufSend[i-1] = V[i][jmax];
      }
  }

  MPI_Sendrecv(bufSend, imax, MPI_DOUBLE, rank_t, chunk, bufRecv, imax, MPI_DOUBLE, rank_b, chunk, MPI_COMM_WORLD, status);

  if (rank_b != MPI_PROC_NULL){
    for (i= 1;i <= imax;i++){
      V[i][0] = bufRecv[i-1];
    }
  }

  /*****************************************/
  /* send to bottom receive from top       */
  /*****************************************/
  if (rank_b != MPI_PROC_NULL){
    for (i= 1; i <= imax; i++)
      {
	bufSend[i-1] = V[i][2];
      }
  }

  MPI_Sendrecv(bufSend, imax, MPI_DOUBLE, rank_b, chunk, bufRecv, imax, MPI_DOUBLE, rank_t, chunk, MPI_COMM_WORLD, status);

  if (rank_t !=MPI_PROC_NULL){
    for (i = 1; i <= imax; i++){
      V[i][jmax+2] = bufRecv[i-1];
    }
  }

}
