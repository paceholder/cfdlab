#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_


typedef struct {
    double Re;
    double UI;
    double VI;
    double PI;
    double GX;
    double GY;
    double t_end;
    double xlength;
    double ylength;
    double dt;
    double dx;
    double dy;
    int  imax;
    int  jmax;
    double alpha;
    double omg;
    double tau;
    int  itermax;
    double eps;
    double dt_value;
    int iproc;
    int jproc;
} Parameters;

#endif
