#include "boundary_val.h"

/**
 * The boundary values of the problem are set.
 * @author Arash Bakhtiari
 */
void boundaryvalues(double **U, double **V, ProcState* procState)
{
    int j;
    int i;

    if (procState->rank_l == MPI_PROC_NULL)
        for ( j = 0 ; j <= procState->jt - procState->jb + 2; ++j)
        {
            /* Initializing the U values lying right on the left and right vertical boundaries */
            U[1][j] = 0 ;
            V[0][j] = - V[1][j] ;
        }

    if (procState->rank_r == MPI_PROC_NULL)
        for( j = 0; j <= procState->jt - procState->jb + 2; ++ j)
        {
            /* Initializing the V values around the left and right vertical boundaries */
            U[procState->ir - procState->il + 2][j] = 0 ;
            V[procState->ir - procState->il + 2][j] = - V[procState->ir - procState->il + 1][j] ;
        }

    if (procState->rank_b == MPI_PROC_NULL)
        for( i = 0; i <= procState->ir - procState->il + 2; ++i )
        {
            /* Initializing the V values lying right on the up and down horizontal boundaries */
            U[i][0] = - U[i][1];
            V[i][1] = 0;
        }

    if (procState->rank_t == MPI_PROC_NULL )
        for( i = 0; i <= procState->ir - procState->il + 2; ++i)
        {
            /* here we apply new boundary conditions for moving upper lid */
            U[i][procState->jt - procState->jb + 2] = 2.0 - U[i][procState->jt - procState->jb + 1];
            V[i][procState->jt - procState->jb + 2] = 0 ;
        }
}

