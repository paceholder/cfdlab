#ifndef _PROCSTATE_H_
#define _PROCSTATE_H_

typedef struct
{
    int il;     /* left border cell number in main grid */
    int ir;     /* right border cell number in main grid */
    int jb;     /* bottom border cell number in main grid */
    int jt;     /* top border cell number in main grid */
    int rank_l; /* rank of left neighbor */
    int rank_r; /* same for right */
    int rank_b; /* same for bottom */
    int rank_t; /* same for top */
    int omg_i;  /* x-number of current subdomain */
    int omg_j;  /* y-number of current subdomain */
    int myrank; /* rank of current process */
    int num_proc; /* total number of processes */
} ProcState;

#endif
