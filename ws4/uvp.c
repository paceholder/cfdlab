#include "uvp.h"

#include <stdlib.h> /*for abs*/
#include <math.h>
#include <stdio.h>
#include "init.h"



void calculate_fg(
  Parameters* parameters,
  double **U,
  double **V,
  double **F,
  double **G,
  ProcState* procState
)
{
    /* 
     * indexing [row][col]
     * j - row
     * i - column 
     */

    int i;
    int j;
    double dx = parameters->dx;
    double dy = parameters->dy;
    double alpha = parameters->alpha;
    double Re = parameters->Re;
    double GX = parameters->GX;
    double GY = parameters->GY;
    double dt = parameters->dt;
    int is;
    int ie;
    int js;
    int je;
    int imax = procState->ir - procState->il + 1;
    int jmax = procState->jt - procState->jb + 1;

    is = (procState->rank_l == MPI_PROC_NULL? 2 : 1 );
    ie = (procState->rank_r == MPI_PROC_NULL ? imax : imax + 1);

     /* central part of the domain */
    /* #pragma omp parallel private(i, j) shared(F) num_threads(2) */
    for(i = is; i <= ie; ++i)
        for(j = 1; j <= jmax; ++j)
        {
            double d2udx2 = (U[i+1][j] - 2 * U[i][j] + U[i-1][j]) / (dx * dx);
            double d2udy2 = (U[i][j+1] - 2 * U[i][j] + U[i][j-1]) / (dy * dy);

            double du2dx = 1/dx * (pow((U[i][j] + U[i+1][j])/2.0, 2.0) - pow((U[i-1][j] + U[i][j])/2.0, 2.0)) +
                    alpha / dx * (fabs(U[i][j] + U[i+1][j])/2.0 * (U[i][j] - U[i+1][j])/2.0 - fabs(U[i-1][j] + U[i][j])/2.0 * (U[i-1][j] - U[i][j])/2.0);

            double duvdy = 1/dy * ( (V[i-1][j+1] + V[i][j+1])/2.0 * (U[i][j] + U[i][j+1])/2.0 - (V[i-1][j] + V[i][j])/2.0 * (U[i][j-1] + U[i][j])/2.0) +
                    alpha / dy * (fabs(V[i-1][j+1] + V[i][j+1])/2.0 * (U[i][j] - U[i][j+1])/2.0 - fabs(V[i-1][j] + V[i][j])/2.0 * (U[i][j-1] - U[i][j])/2.0);

            F[i][j] = U[i][j] + parameters->dt * (1/Re * (d2udx2 + d2udy2) - du2dx - duvdy + GX);
        }

    js = (procState->rank_b == MPI_PROC_NULL ? 2 : 1 );
    je = (procState->rank_t == MPI_PROC_NULL ? jmax : jmax + 1);

    /* #pragma omp parallel private(i, j) shared(G) num_threads(2) */
    for(i = 1; i <= imax; ++i)
        for(j = js; j <= je; ++j)
        {
            double d2vdx2 = (V[i+1][j] - 2 * V[i][j] + V[i-1][j]) / (dx * dx);
            double d2vdy2 = (V[i][j+1] - 2 * V[i][j] + V[i][j-1]) / (dy * dy);

            double dv2dy = 1/dy * (pow((V[i][j] + V[i][j+1])/2.0, 2.0) - pow((V[i][j-1] + V[i][j])/2.0, 2.0)) +
                    alpha / dy * (fabs(V[i][j] + V[i][j+1])/2.0 * (V[i][j] - V[i][j+1])/2.0 - fabs(V[i][j-1] + V[i][j])/2.0 * (V[i][j-1] - V[i][j])/2.0);

            double duvdx = 1/dx * ( (U[i+1][j-1] + U[i+1][j])/2.0 * (V[i][j] + V[i+1][j])/2.0 - (U[i][j-1] + U[i][j])/2.0 * (V[i-1][j] + V[i][j])/2.0) +
                    alpha / dx * (fabs(U[i+1][j-1] + U[i+1][j])/2.0 * (V[i][j] - V[i+1][j])/2.0 - fabs(U[i][j-1] + U[i][j])/2.0 * (V[i-1][j] - V[i][j])/2.0);

            G[i][j] = V[i][j] + dt * (1/Re * (d2vdx2 + d2vdy2) - duvdx - dv2dy + GY);
        }

    for(j=1; j<=jmax; j++)
      {
        F[is-1][j]=U[is-1][j];
        F[ie+1][j]=U[ie+1][j];
      }
    
    for(i=1; i<=imax; i++)
      {
        G[i][js-1]=V[i][js-1];
        G[i][je+1]=V[i][je+1];
      }
}



void calculate_rs(Parameters* parameters, double **F, double **G, double **RS, ProcState* procState)
{
    int i, j;
    double dx = parameters->dx;
    double dy = parameters->dy;
    double dt = parameters->dt;

    int imax = procState->ir - procState->il + 1;
    int jmax = procState->jt - procState->jb + 1;

    for(i = 2; i <= imax+1; ++i)
    {
        for(j = 2; j <= jmax+1; ++j)
        {
            /* 
             * perhaps a problem that the boundary is not set for RS 
             * (only from 1 to jmax and not from 0 to jmax+1; 
             * depends on how we need jt and how its used
             */
            RS[i-1][j-1] = ( (F[i][j-1] - F[i-1][j-1])/dx + (G[i-1][j] - G[i-1][j-1])/dy ) / dt;
        }
    }
}



void calculate_dt(Parameters* parameters, ProcState* procState, double **U, double **V)
{
    double Re = parameters->Re;
    double dx = parameters->dx;
    double dy = parameters->dy;
    double tau = parameters->tau;
    int i,j;
    double umax = 0;
    double vmax = 0;

    double tmp;
    double min3;
    double velocity[2];
    double maxvelocity[2];
    int h = procState->jt - procState->jb + 1;
    int w = procState->ir - procState->il + 1;


    for(i = 1; i <= w+1; ++i) 
    {
        /*check if jts < or <= and if it should be imax+1 or something else*/
        for(j = 1; j <= h+1; ++j)
        {
            if ( fabs(U[i][j]) > umax )
                umax = fabs(U[i][j]);
        }
    }

    for(i = 1; i <= procState->ir - procState->ir + 2; ++i)
    {
        /*check if jts < or <= and if it should be imax+1 or something else*/
        for(j = 1; j <= procState->jt - procState->jb + 2; ++j)
        {
            if( fabs(V[i][j]) > vmax)
                vmax = fabs(V[i][j]);
        }
    }
    

    velocity[0] = umax;
    velocity[1] = vmax;

    /* searches for maximal speed */
    MPI_Reduce(velocity, maxvelocity, 2, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

    if (procState->myrank == 0)
    {
        /* not possjble to have a min wjth 3 inputs so min of 2 and then min of min*/
        tmp = fmin((0.5 * Re/(1/(dx*dx)+1/(dy*dy))),(dx/maxvelocity[0]));

        min3 = fmin((dy/maxvelocity[1]), tmp);
        parameters->dt = tau*min3;

    }
    /* for root = 0 process  jt is sender, and receiver for others */
    MPI_Bcast(&parameters->dt, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
}

/**
 * Calculates the new velocity values according to the formula
 *
 * @f$ u_{i,j}^{(n+1)}  =  F_{i,j}^{(n)} - \frac{\delta t}{\delta x} (p_{i+1,j}^{(n+1)} - p_{i,j}^{(n+1)}) @f$
 * @f$ v_{i,j}^{(n+1)}  =  G_{i,j}^{(n)} - \frac{\delta t}{\delta y} (p_{i,j+1}^{(n+1)} - p_{i,j}^{(n+1)}) @f$
 *
 * As always the index range is
 *
 * @f$ i=1,\ldots,imax-1, \quad j=1,\ldots,jmax @f$
 * @f$ i=1,\ldots,imax, \quad j=1,\ldots,jmax-1 @f$
 *
 * @image html calculate_uv.jpg
 * @author Arash Bakhtiari
 */
void calculate_uv(
    Parameters *parameters,
    double **U,
    double **V,
    double **F,
    double **G,
    double **P, 
    ProcState* procState,
    double *bufSend,
    double *bufRecv

)
{
  int i;
  int j;
  double dx = parameters->dx;
  double dy = parameters->dy;
  double dt = parameters->dt;

  int is;
  int ie;
  int js;
  int je;

  int imax = procState->ir - procState->il + 1;
  int jmax = procState->jt - procState->jb + 1;

  is = (procState->rank_l == MPI_PROC_NULL ? 2 : 1 );
  ie = (procState->rank_r == MPI_PROC_NULL ? imax : imax +1 );

  /* Computing the U at the next time step */
    for ( i = is ; i <= ie ; i++)
    {
        for ( j = 1; j <= jmax ; j++ )
        {
            U[i][j] = F[i][j] - (dt/dx)*( P[i][j] - P[i-1][j] ) ;
        }
    }

    js = (procState->rank_b == MPI_PROC_NULL ? 2 : 1 );
    je = (procState->rank_t == MPI_PROC_NULL ? jmax : jmax + 1);

    /* Computing the V at the next time step */
    for ( i = 1 ; i <= imax ; i++)
    {
        for ( j = js ; j <= je ; j++ )
        {
            V[i][j] = G[i][j] - (dt/dy)*( P[i][j] - P[i][j-1] ) ;
        }
    }


}

