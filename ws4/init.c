#include "helper.h"
#include "init.h"

int read_parameters( const char *szFileName,       /* name of the file */
                     Parameters* parameters)
{
    double xlength;
    double ylength;
    double Re;
    double t_end;
    double dt;
    int imax;
    int jmax;
    double omg;
    double eps;
    double tau;
    double alpha;
    int itermax;
    double dt_value;
    double UI;
    double VI;
    double GX;
    double GY;
    double PI;
    int iproc;
    int jproc;

   READ_DOUBLE( szFileName, xlength );
   READ_DOUBLE( szFileName, ylength );

   READ_DOUBLE( szFileName, Re    );
   READ_DOUBLE( szFileName, t_end );
   READ_DOUBLE( szFileName, dt    );

   READ_INT   ( szFileName, imax );
   READ_INT   ( szFileName, jmax );

   READ_DOUBLE( szFileName, omg   );
   READ_DOUBLE( szFileName, eps   );
   READ_DOUBLE( szFileName, tau   );
   READ_DOUBLE( szFileName, alpha );

   READ_INT   ( szFileName, itermax );
   READ_DOUBLE( szFileName, dt_value );

   READ_DOUBLE( szFileName, UI );
   READ_DOUBLE( szFileName, VI );
   READ_DOUBLE( szFileName, GX );
   READ_DOUBLE( szFileName, GY );
   READ_DOUBLE( szFileName, PI );

   READ_INT( szFileName, iproc);
   READ_INT( szFileName, jproc);

   parameters->xlength = xlength;
   parameters->ylength = ylength;
   parameters->Re = Re;
   parameters->t_end = t_end;
   parameters->dt = dt;
   parameters->imax = imax;
   parameters->jmax = jmax;
   parameters->omg = omg;
   parameters->eps = eps;
   parameters->tau = tau;
   parameters->alpha = alpha;
   parameters->itermax = itermax;
   parameters->dt_value = dt_value;
   parameters->UI = UI;
   parameters->VI = VI;
   parameters->GX = GX;
   parameters->GY = GY;
   parameters->PI = PI;

    parameters->iproc = iproc;
    parameters->jproc = jproc;

   parameters->dx = parameters->xlength / (double)(parameters->imax);
   parameters->dy = parameters->ylength / (double)(parameters->jmax);

   return 1;
}


void init_uvp(Parameters* parameters, double** U, double** V, double** P, ProcState* procState)
{
  init_matrix(P, 0, procState->ir - procState->il + 2, 0, procState->jt - procState->jb + 2, parameters->PI);
  init_matrix(U, 0, procState->ir - procState->il + 3, 0, procState->jt - procState->jb + 2, parameters->UI);
  init_matrix(V, 0, procState->ir - procState->il + 2, 0, procState->jt - procState->jb + 3, parameters->VI);
}
