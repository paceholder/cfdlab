#include "helper.h"
#include "visual.h"
#include "init.h"
#include "uvp.h"
#include "sor.h"
#include "boundary_val.h"
#include "parallel.h"

#include <stdio.h>
#include <mpi.h>



/**
 * The main operation reads the configuration file, initializes the scenario and
 * contains the main loop. So here are the individual steps of the algorithm:
 *
 * - read the program configuration file using read_parameters()
 * - set up the matrices (arrays) needed using the matrix() command
 * - create the initial setup init_uvp(), init_flag(), output_uvp()
 * - perform the main loop
 * - trailer: destroy memory allocated and do some statistics
 *
 * The layout of the grid is decribed by the first figure below, the enumeration
 * of the whole grid is given by the second figure. All the unknowns corresond
 * to a two dimensional degree of freedom layout, so they are not stored in
 * arrays, but in a matrix.
 *
 * @image html grid.jpg
 *
 * @image html whole-grid.jpg
 *
 * Within the main loop the following big steps are done (for some of the
 * operations a definition is defined already within uvp.h):
 *
 * - calculate_dt() Determine the maximal time step size.
 * - boundaryvalues() Set the boundary values for the next time step.
 * - calculate_fg() Determine the values of F and G (diffusion and confection).
 *   This is the right hand side of the pressure equation and used later on for
 *   the time step transition.
 * - calculate_rs()
 * - Iterate the pressure poisson equation until the residual becomes smaller
 *   than eps or the maximal number of iterations is performed. Within the
 *   iteration loop the operation sor() is used.
 * - calculate_uv() Calculate the velocity at the next time step.
 */
int main(int argc, char** argv)
{
    const char* szFileName  = "cavity100.dat";
    const char* outputFileName = "out.vtk";
    Parameters parameters;
    double** U;
    double** V;
    double** F;
    double** G;
    double** RS;
    double** P;
    ProcState procState;
    int max_length;
    int rank;
    double t = 0.0;
    int n = 0;
    double *bufSend;
    double *bufRecv;

    /* MPI initialization */
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    /* first read parameters ... */
    if (rank == 0)
    {
        read_parameters(szFileName, &parameters);
    }

    
    MPI_Bcast(&parameters, sizeof(Parameters), MPI_BYTE, 0, MPI_COMM_WORLD);

    /* get parameters for current process/subdomain */
    init_parallel(&parameters, &procState);

    /* and just then allocation */
    P = matrix(0, procState.ir - procState.il + 2, 0, procState.jt - procState.jb + 2);
    U = matrix(0, procState.ir - procState.il + 3, 0, procState.jt - procState.jb + 2);
    V = matrix(0, procState.ir - procState.il + 2, 0, procState.jt - procState.jb + 3);
    F = matrix(0, procState.ir - procState.il + 3, 0, procState.jt - procState.jb + 2);
    G = matrix(0, procState.ir - procState.il + 2, 0, procState.jt - procState.jb + 3);
    RS= matrix(0, procState.ir - procState.il + 2, 0, procState.jt - procState.jb + 2);

    /* assign initial values */
    init_uvp(&parameters, U, V, P, &procState);

    max_length = procState.ir-procState.il+3;
    if(max_length < procState.jt-procState.jb+3)
      {
	max_length = procState.jt-procState.jb+3;    
      }
    
    bufSend = (double *) malloc((size_t)(max_length * sizeof(double)));
    bufRecv = (double *) malloc((size_t)(max_length * sizeof(double)));

    while (t < parameters.t_end)
    {
        int it = 0;
        double res = 1.0;


        /* it finds max velocity in all the subdomains and then estimates dt */
        calculate_dt(&parameters, &procState, U, V);

        /* it is modified for parallel work now */
        boundaryvalues(U, V, &procState);

        calculate_fg(&parameters, U, V, F, G, &procState);

        calculate_rs(&parameters, F, G, RS, &procState);

        /* sor iteration */
        while (it < parameters.itermax && res > parameters.eps)
        {
	  sor(&parameters, P, RS, &res, &procState,bufSend,bufRecv,it);
	  ++it;
        }

        calculate_uv(&parameters, U, V, F, G, P, &procState,bufSend,bufRecv);
	uv_comm( U, V, &procState,bufSend, bufRecv , n);

        if (n % 100 == 0)
            output_uvp(U, V, P, &procState, outputFileName, n, &parameters);

        t += parameters.dt;
        ++n;


    }


    /* clean up memory */
    free_matrix(U, 0, procState.ir -  procState.il + 3, 0,  procState.jt -  procState.jb + 2); 
    free_matrix(V, 0,  procState.ir -  procState.il + 2, 0,  procState.jt -  procState.jb + 3); 
    free_matrix(P, 0,  procState.ir -  procState.il + 2, 0,  procState.jt -  procState.jb + 2);
    free_matrix(F, 0,  procState.ir -  procState.il + 3, 0,  procState.jt -  procState.jb + 2); 
    free_matrix(G, 0,  procState.ir -  procState.il + 2, 0,  procState.jt -  procState.jb + 3); 
    free_matrix(RS, 0,  procState.ir - procState.il + 2, 0,  procState.jt -  procState.jb + 2);
    
    free(bufSend);
    free(bufRecv);

    Programm_Stop("Program stops");
    return 0;

}
